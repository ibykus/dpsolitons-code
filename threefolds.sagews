︠b98be644-c28c-469e-b05e-6e4364bde0c2i︠
%md
### Smooth Fano Threefolds with 2-Torus Action

1. Q
2. 2.24
3. 2.29
4. [2.30](#230)
5. [2.31](#231)
6. 2.32
7. [3.8](#38)
8. 3.10
9. [3.18](#318)
10. 3.19
11. 3.20
12. [3.21](#321)
13. [3.22](#322)
14. [3.23](#323)
15. [3.24](#324)
16. 4.4
17. [4.5](#45)
18. 4.7
19. [4.8](#48)
︡dc57548e-355b-4236-9cfe-d925ea5b893b︡{"done":true,"md":"### Smooth Fano Threefolds with 2-Torus Action\n\n1. Q\n2. 2.24\n3. 2.29\n4. [2.30](#230)\n5. [2.31](#231)\n6. 2.32\n7. [3.8](#38)\n8. 3.10\n9. [3.18](#318)\n10. 3.19\n11. 3.20\n12. [3.21](#321)\n13. [3.22](#322)\n14. [3.23](#323)\n15. [3.24](#324)\n16. 4.4\n17. [4.5](#45)\n18. 4.7\n19. [4.8](#48)"}
︠6a5ea377-8f2a-4af3-8454-852a1fb5c749i︠
%auto
%hide
# The function degenerations:
# Returns the list of degneration polytopes of given combinatorial data of a T-variety. 
# Takes inputs L and B, where L is a list of matrices and B is the base polytope. 
# Each matrix in L represents a piecewise affine function on B in the following way:

#Suppose $v_j$ are the columns of $M$ in the list $L$. We then form the piecewise affine function
# $\phi_M(x_1,\dots,x_n) := \min_j \{ (1,x_1,x_2,\dots,x_n) \cdot v_j \} $.

def degenerations(L,B):
    dim = L[0].nrows()
    genericfunction = matrix(QQ, dim, 1, lambda i, j: 0)
    L = L +[genericfunction]
    def shift(A,r):
        shift = matrix(QQ,  A.transpose().nrows() , 1, lambda i, j: r);
        zeroes = matrix(QQ, A.transpose().nrows(), A.nrows() -1 , lambda i, j: 0);
        return A + shift.augment(zeroes).transpose()
    def minsum(L):
        msCurrent = L[0]
        def minsum_step(A1,A2):
            C = matrix(QQ,A2.nrows(),1,lambda i, j: 0)
            for c in A1.columns():
                for d in A2.columns():
                    C = C.augment(d+c);
            return C.delete_columns([0])
        for l in L[1:]:
            msCurrent = minsum_step(msCurrent,l)
        return msCurrent
    def degenerations_step(L,k,B):
        up = shift(L[k],1)
        base = matrix(QQ, B.inequalities_list()[:]);
        low = L[:]; low.pop(k);
        low = minsum(low).transpose();
        up = up.transpose()
        base = base.augment(matrix(QQ,  base.nrows() , 1, lambda i, j: 0)).transpose()
        up = up.augment(matrix(QQ, up.nrows(), 1, lambda i, j: -1)).transpose()
        low = low.augment(matrix(QQ, low.nrows(), 1, lambda i, j: 1)).transpose()
        low = shift(low,1)
        P = Polyhedron(ieqs = [list(v) for v in up.augment(low.augment(base)).columns()])
        return P;
    Polyhedra = [];
    for k in range(len(L)):
        Polyhedra = Polyhedra + [degenerations_step(L,k,B)];
    return Polyhedra


def projection(P,F):
    A = F.as_polyhedron().equations()[0].A()
    b = F.as_polyhedron().equations()[0].b()
    A = list(A)
    B=vector(QQ,A)
    B=denominator(B)*B
    A = list(B)
    B = matrix(ZZ,B)
    K = B.right_kernel()
    if P.dimension() == 1:
        K= matrix(ZZ,[0])
    else:
        K = matrix(ZZ,K.basis())
    return [K,A]

def acoeff(F,c):
    v_0 = F.vertices()[0].vector(); v_0
    return exp(c.dot_product(v_0))

def bcoeff(F,v):
    u_0 = F.vertices()[0].vector()
    return v.dot_product(u_0)

def relvolume(P):
    if P.dimension() == 0:
        return 1
    else:
        return P.volume()


# The function intexp:
# Calculates the integral of exp(c*x) dx over the polyhedron P recursively using the Barvinok method. Input is a polyhedron P,
# a sufficiently general vector L (not orthogonal, after projection, to c or any face of P at any stage of the recursion),
# and a vector c for the integrand. To optimize performance values obtained at faces are cached along the way.
def intexp(P,L,c,face=None, cache=None):
    if face is None: face = tuple(range(P.n_vertices()))
    if cache is None:
        cache = {}
    if cache.has_key(frozenset(face)):
        return cache[frozenset(face)]
    I = 0
    if c.is_zero():
        cache[face]= relvolume(P)
        return relvolume(P)
    else:
        for F in P.faces(P.dimension() -1):
            ProjMatrix,A = projection(P,F)
            n_F = -F.ambient_Hrepresentation()[0].A()
            n_F = n_F/gcd(n_F)
            coeff = acoeff(F,c)*(1/L.dot_product(c))*(L.dot_product(n_F))
            c_F = ProjMatrix*c
            L_F = ProjMatrix.insert_row(0,vector(ZZ,A)).transpose().solve_right(L)[1:]
            v_0 = F.vertices()[0].vector(); v_0
            Vert = [ProjMatrix.transpose().solve_right(v.vector() - v_0)  for v in F.vertices() ]
            face_F = tuple([face[i] for i in F._ambient_Vrepresentation_indices])
            P_F = Polyhedron(vertices = Vert, backend="cdd")
            I = I + coeff*intexp(P_F,L_F,c_F,face_F,cache)
    cache[frozenset(face)]=I
    return I

# The function intxexp:
# Calculates the integral of <x,v>*exp(<x,c>) dx over the polyhedron P recursively using the Barvinok method. Input is a polyhedron P,
# a sufficiently general vector L (not orthogonal, after projection, to c or any face of P at any stage of the recursion),
# and vectors c and v for the integrand. To optimize performance values obtained at faces are cached along the way.
def intxexp(P,L,c,v,face=None,cache=None,cache2=None):
    if face is None: face = tuple(range(P.n_vertices()))
    if cache2 is None:
        cache2 = {}
    if cache is None:
        cache = {}
    if cache.has_key(frozenset(face)):
        return cache[frozenset(face)] 
    I = 0
    if c.is_zero():
        if v.is_zero() : return 0
        for T in list(P.triangulate()):
            T = [P.Vrepresentation()[ZZ(t)].vector() for t in T]
            bary = sum(T)/len(T)
            T = Polyhedron(vertices = T)
            I = I + T.volume()*bary.dot_product(v)
    else:
        for F in P.faces(P.dimension() -1):
                ProjMatrix = projection(P,F)
                L_F  = ProjMatrix[0].insert_row(0,vector(ZZ,ProjMatrix[1])).transpose().solve_right(L)[1:]
                c_F = ProjMatrix[0]*c
                v_F = ProjMatrix[0]*v
                v_0 = F.vertices()[0].vector(); v_0
                Vert = [ProjMatrix[0].transpose().solve_right(w.vector() - v_0)  for w in F.vertices() ]
                face_F = tuple([face[i] for i in F._ambient_Vrepresentation_indices])
                P_F = Polyhedron(vertices = Vert,backend="cdd")
                n_F = -F.ambient_Hrepresentation()[0].A()
                n_F = n_F/gcd(n_F)
                I = I + (L.dot_product(n_F)/L.dot_product(c))*acoeff(F,c)*(intxexp(P_F,L_F,c_F,v_F,face_F,cache,cache2) + bcoeff(F,v)*intexp(P_F,L_F,c_F,face_F,cache2)) - L.dot_product(n_F)*(v.dot_product(L)/(L.dot_product(c)^2))*acoeff(F,c)*intexp(P_F,L_F,c_F,face_F,cache2)
    cache[frozenset(face)]=I
    return I

%md Here some helper functions are defined. These are `degenerations()` for calculating the toric special fibres of the test configurations and `intxexp()` to solve the integrals $\int_\Delta \langle u, v \rangle e^{\langle u, \xi \rangle} du$ analytically. <br> **Click &#x25B8; on the left to unhide.**


︡1fbc37b3-d6ed-4baf-838c-b40349eb56d9︡{"hide":"input"}︡{"md":"Here some helper functions are defined. These are `degenerations()` for calculating the toric special fibres of the test configurations and `intxexp()` to solve the integrals $\\int_\\Delta \\langle u, v \\rangle e^{\\langle u, \\xi \\rangle} du$ analytically. <br> **Click &#x25B8; on the left to unhide.**"}︡{"done":true}︡
︠5b95d309-fcc6-4c02-b1b1-bd08ba454e20i︠
%md 
---
#### No. 2.30
<a id="230"></a>
The combinatorial data is is given by $\Box= \text{conv} (\, (-3,0),(-2,1),(2,1),(3,0),(0,-3) \, )$ and  $\Phi_0(x,y) = \min \{0,-x\},\; \Phi_1(x,y)=  \min \{0,y\},\; \Phi_\infty(x,y) = \frac{x-y-1}{2}$.
︡d84b3532-a032-4b99-9343-c7ca2eb6929a︡{"done":true,"md":"---\n#### No. 2.30\n<a id=\"230\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\, (-3,0),(-2,1),(2,1),(3,0),(0,-3) \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,-x\\},\\; \\Phi_1(x,y)=  \\min \\{0,y\\},\\; \\Phi_\\infty(x,y) = \\frac{x-y-1}{2}$."}
︠a207f1b7-ad6f-46a1-9b34-5c27f1d99d6cs︠
# We are working with interval arithmetic with precision of 16 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-3,0],[-2,1],[2,1],[3,0],[0,-3]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,-1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,0,1]]).transpose()
Phi3 = matrix(QQ,[[-1/2,1/2,-1/2]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡df5149e9-ddab-49c7-83ea-67b4e3fdb8c5︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/23207/tmp_aguSfW.svg","show":true,"text":null,"uuid":"69eae8aa-2623-48f5-8cd3-9cccc48da470"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, y\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\frac{1}{2} \\, x - \\frac{1}{2} \\, y - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠4f182eb3-b368-4cd8-a3f8-48e3fc474383si︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix}-1 & 0\\ 0 & 1\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence, we have $\xi=(0,\xi_2)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, (0,\xi_2) \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (0,\xi_2,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡a00c63aa-01bf-49b0-8001-a16d28b654cf︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix}-1 & 0\\\\ 0 & 1\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence, we have $\\xi=(0,\\xi_2)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, (0,\\xi_2) \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (0,\\xi_2,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠c62f6512-7874-456a-9fea-65de6343246ds︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,3,13])

# Analytic solution of the integral
F1(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([0,1,0])).simplify_full()

show(vector([F1,F2]))
︡a5fa40d6-6901-41c4-a7b0-5d70e7aae269︡{"html":"<div align='center'>$\\displaystyle \\xi_{2} \\ {\\mapsto}\\ \\left(0,\\,\\frac{{\\left({\\left(2 \\, \\xi_{2}^{3} - 3 \\, \\xi_{2} - 3\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 12 \\, \\xi_{2} e^{\\left(3 \\, \\xi_{2}\\right)} + 3 \\, \\xi_{2} + 3\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{\\xi_{2}^{4}}\\right)$</div>"}︡{"done":true}︡
︠74b37840-a13c-472e-b59a-f53d714cac59i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡9a8f4134-bdbf-49c6-9ad2-b4dd82c2b178︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠acfba0c7-c59b-4738-812c-2dbefa28a472s︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
lower = 0.514
upper = 0.515

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡8ffa6ca1-811e-4821-a16e-c193fe29642c︡{"stdout":"Interval containing solution: [0.513992 .. 0.515015]\n"}︡{"done":true}︡
︠a3a40b0c-03c1-47f1-a758-64fdb17d1aafi︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every (admissible) choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡eef5b155-e0f8-4482-b2a2-dfbf0073982b︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every (admissible) choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}
︠b31a8b61-6314-4e17-837f-384195b5a5a0s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([0,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡fccc7901-2f0f-409c-a0e5-b50790a31c2a︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi_{2}^{3} - 3 \\, \\xi_{2} - 3\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 3 \\, {\\left(3 \\, \\xi_{2}^{2} + 2\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 3 \\, \\xi_{2} - 3\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{3 \\, \\xi_{2}^{4}} \\geq 1.088$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(8 \\, \\xi_{2}^{3} + 6 \\, \\xi_{2}^{2} - 3\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} - 12 \\, {\\left(3 \\, \\xi_{2}^{2} - 3 \\, \\xi_{2} + 1\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 12 \\, \\xi_{2} + 15\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{6 \\, \\xi_{2}^{4}} \\geq 2.178$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, {\\left(2 \\, \\xi_{2}^{3} - 3 \\, \\xi_{2} - 3\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} - 3 \\, {\\left(3 \\, \\xi_{2}^{2} - 12 \\, \\xi_{2} + 2\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 12 \\, \\xi_{2} + 12\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{6 \\, \\xi_{2}^{4}} \\geq 0.4465$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(8 \\, \\xi_{2}^{3} + 6 \\, \\xi_{2}^{2} - 3\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} - 3 \\, {\\left(3 \\, \\xi_{2}^{2} - 2\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 6 \\, \\xi_{2} - 3\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{6 \\, \\xi_{2}^{4}} \\geq 4.151$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠2c4f31e0-496e-493f-ad4d-9f3f38f64ff2is︠
%hide
%md 
---
#### No. 2.31
<a id="231"></a>
The combinatorial data is is given by $\Box= \text{conv} (\,(-3,0),(-1,2),(0,2),(2,0),(2,-1),(0,-3)  \, )$ and  $\Phi_0(x,y) = \min \{0,-x\},\; \Phi_1(x,y)= \min \{0,y\} ,\; \Phi_\infty(x,y) = \min \{-y, \frac{x-y-1}{2} \}$.
︡26bb471d-2ae2-4c77-adaf-cd42f188a480︡{"hide":"input"}︡{"md":"---\n#### No. 2.31\n<a id=\"231\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\,(-3,0),(-1,2),(0,2),(2,0),(2,-1),(0,-3)  \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,-x\\},\\; \\Phi_1(x,y)= \\min \\{0,y\\} ,\\; \\Phi_\\infty(x,y) = \\min \\{-y, \\frac{x-y-1}{2} \\}$."}︡{"done":true}︡
︠8ead6acd-b66d-4d5d-a465-7d4a3eab71dbs︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(60)

# The base polytope
B=Polyhedron(vertices = [[-3,0],[-1,2],[0,2],[2,0],[2,-1],[0,-3]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,-1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,0,1]]).transpose()
Phi3 = matrix(QQ,[[-1/2,1/2,-1/2],[0,0,-1]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡659b9d7d-ff1a-4a9d-b30a-305311b530fc︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_g3vT22.svg","show":true,"text":null,"uuid":"9613993a-0d76-4073-8da8-e1d26a0d47f9"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, y\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(\\frac{1}{2} \\, x - \\frac{1}{2} \\, y - \\frac{1}{2}, -y\\right)$</div>"}︡{"done":true}︡
︠8bf0f4b5-6e65-40a8-874b-5333a7194c7eis︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
 For this note, that $\left(\begin{smallmatrix} 0 & 1\\ 1 & 0\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence, we have $\xi=(\xi_1,\xi_1)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡ce3eb147-c166-455a-9be1-978810a6904b︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\n For this note, that $\\left(\\begin{smallmatrix} 0 & 1\\\\ 1 & 0\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence, we have $\\xi=(\\xi_1,\\xi_1)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠8f050457-2de5-401d-9923-114150c05239s︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,3,13])

# Analytic solution of the integral
F1(xi_1) = intxexp(Polyhedra[0],Lambda,vector([xi_1,xi_1,0]),vector([1,-1,0])).simplify_full()
F2(xi_1) = intxexp(Polyhedra[0],Lambda,vector([xi_1,xi_1,0]),vector([1,1,0])).simplify_full()

show(vector([F1,F2]))
︡a4a84787-affb-40ed-a437-cdd1113bcb8c︡{"html":"<div align='center'>$\\displaystyle \\xi_{1} \\ {\\mapsto}\\ \\left(0,\\,-\\frac{{\\left(9 \\, \\xi_{1}^{2} - 8 \\, {\\left(\\xi_{1}^{2} - \\xi_{1}\\right)} e^{\\left(5 \\, \\xi_{1}\\right)} + {\\left(3 \\, \\xi_{1}^{2} - 5 \\, \\xi_{1} - 3\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} + 9 \\, \\xi_{1} + 3\\right)} e^{\\left(-3 \\, \\xi_{1}\\right)}}{2 \\, \\xi_{1}^{4}}\\right)$</div>"}︡{"done":true}︡
︠9432975e-3c6f-470b-88a9-080032b48f35i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡f4a73aa8-47b7-4bd2-b7b1-0233ab8b80ca︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠2ccd2f71-e370-4cbe-85b4-87b2814410bdss︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
lower = 0.285497
upper = 0.285507

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡6d2a1441-16d2-4e81-b439-b59773e30037︡{"stdout":"Interval containing solution: [0.28549700000000000077 .. 0.28550700000000001078]\n"}︡{"done":true}︡
︠b127af9e-5b07-4d3b-b6db-4376d47ce1f0i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every (admissible) choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡afb3526f-cf29-4d6e-9f26-92af7e6d47d1︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every (admissible) choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}
︠e98ed8dd-1117-4829-82e8-658df3d4c570s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([xi_2,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡4c256dbe-8947-4750-9c9a-5d1d3e3444bc︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(12 \\, \\xi_{2}^{2} + {\\left(4 \\, \\xi_{2}^{2} + 2 \\, \\xi_{2} - 3\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 10 \\, \\xi_{2} + 3\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{8 \\, \\xi_{2}^{4}} \\geq 1.9509141924719434162$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(6 \\, \\xi_{2}^{2} - 16 \\, {\\left(\\xi_{2}^{2} - \\xi_{2}\\right)} e^{\\left(5 \\, \\xi_{2}\\right)} + {\\left(2 \\, \\xi_{2}^{2} - 12 \\, \\xi_{2} - 3\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 8 \\, \\xi_{2} + 3\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{8 \\, \\xi_{2}^{4}} \\geq 1.9410579647693620622$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi_{2}^{2} - {\\left(3 \\, \\xi_{2}^{2} + 4 \\, \\xi_{2} + 2\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 4 \\, \\xi_{2} + 4 \\, e^{\\left(5 \\, \\xi_{2}\\right)} - 4 \\, e^{\\left(3 \\, \\xi_{2}\\right)} + 2\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{4 \\, \\xi_{2}^{4}} \\geq 0.46321187855599924134$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi_{2}^{2} + 4 \\, {\\left(2 \\, \\xi_{2}^{2} - 2 \\, \\xi_{2} + 1\\right)} e^{\\left(5 \\, \\xi_{2}\\right)} - {\\left(2 \\, \\xi_{2}^{2} - 3 \\, \\xi_{2} + 2\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 5 \\, \\xi_{2} - 4 \\, e^{\\left(3 \\, \\xi_{2}\\right)} + 2\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{4 \\, \\xi_{2}^{4}} \\geq 4.3703749450045982306$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠ff3f5dd6-8173-41a4-a0e7-da9e2d64e0e6i︠
%md 
---
#### No. 3.8*
<a id="38"></a>
The combinatorial data is is given by $\Box= \text{conv} (\,(-2,-1),(-2,1),(1,1),(3,-1),(3,-2),(-1,-2)  \, )$ and  $\Phi_0(x,y) = \min \{0,\frac{-x-1}{2}\},\; \Phi_1(x,y)=  \min \{0,\frac{-y-1}{2}\} ,\; \Phi_\infty(x,y) = \min \{0, \frac{x+y-1}{2}\}$.
︡6eef2cd7-9d58-46aa-9950-41e719dc8f16︡{"done":true,"md":"---\n#### No. 3.8*\n<a id=\"38\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\,(-2,-1),(-2,1),(1,1),(3,-1),(3,-2),(-1,-2)  \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,\\frac{-x-1}{2}\\},\\; \\Phi_1(x,y)=  \\min \\{0,\\frac{-y-1}{2}\\} ,\\; \\Phi_\\infty(x,y) = \\min \\{0, \\frac{x+y-1}{2}\\}$."}
︠93705414-52e1-4406-b38d-e3baed0f894as︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(60)

# The base polytope
B=Polyhedron(vertices = [[-2,-1],[-2,1],[1,1],[3,-1],[3,-2],[-1,-2]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[-1/2,-1/2,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[-1/2,0,-1/2]]).transpose()
Phi3 = matrix(QQ,[[0,0,0],[-1/2,1/2,1/2]]).transpose();


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡197e5535-fa92-4c87-8261-6ec4c83cfdbd︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_Zmoszo.svg","show":true,"text":null,"uuid":"5dd02f16-fb8b-48c6-a079-9de57ebd2777"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -\\frac{1}{2} \\, x - \\frac{1}{2}\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -\\frac{1}{2} \\, y - \\frac{1}{2}\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, \\frac{1}{2} \\, x + \\frac{1}{2} \\, y - \\frac{1}{2}\\right)$</div>"}︡{"done":true}︡
︠91db3862-29af-4986-8806-e2fc0be5b69eis︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix}-1 & -1\\ 0 & 1\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence, we have $\xi=(0,\xi_2)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡f9bf86b8-f592-4c83-a1c9-c7f8ad62e2be︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix}-1 & -1\\\\ 0 & 1\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence, we have $\\xi=(0,\\xi_2)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠3e65ec89-f793-4470-a7ac-d412f32d877cs︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,5,13])

# Analytic solution of the integral
F1(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([0,1,0])).simplify_full()

show(vector([F1,F2]))
︡1122db7c-aecf-48ca-b057-4199256f4a70︡{"html":"<div align='center'>$\\displaystyle \\xi_{2} \\ {\\mapsto}\\ \\left(-\\frac{{\\left({\\left(2 \\, \\xi_{2}^{2} - \\xi_{2} - 2\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 5 \\, {\\left(\\xi_{2} + 2\\right)} e^{\\xi_{2}} - 8 \\, \\xi_{2} - 8\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{4 \\, \\xi_{2}^{3}},\\,\\frac{{\\left({\\left(2 \\, \\xi_{2}^{2} - \\xi_{2} - 2\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 5 \\, {\\left(\\xi_{2} + 2\\right)} e^{\\xi_{2}} - 8 \\, \\xi_{2} - 8\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{2 \\, \\xi_{2}^{3}}\\right)$</div>"}︡{"done":true}︡
︠e038cc8d-79b9-4084-9452-ebbe59bd8e1bi︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡8c8daba2-db9d-4ff7-8c2b-61864575e083︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠f09fe1b3-076e-49a1-8d05-2321b40812b1s︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
find_root(F2,-10,10)

lower = 0.799716
upper = 0.799717

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡bae7955a-dc9e-4888-99b8-0e66047e0e1c︡{"stdout":"0.7997160277154249\n"}︡{"stdout":"Interval containing solution: [0.79971599999999998242 .. 0.79971700000000001119]\n"}︡{"done":true}︡
︠6a61c5fe-4ec4-4f75-b156-87030de99c94is︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡9f542c72-b4c9-4338-99b0-9db11826252c︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠149ba5be-2785-4ad7-a169-2b1694d6e792s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([0,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡4f35079d-4983-4379-97af-e8768c60c42a︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi_{2}^{2} - 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 2 \\, \\xi_{2} + 1\\right)} e^{\\left(-\\xi_{2}\\right)}}{4 \\, \\xi_{2}^{3}} \\geq 0.87460114755231644$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(48 \\, \\xi_{2}^{2} - {\\left(5 \\, \\xi_{2}^{3} - 9 \\, \\xi_{2}^{2} - 12 \\, \\xi_{2}\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 6 \\, {\\left(10 \\, \\xi_{2}^{2} + 5 \\, \\xi_{2} + 1\\right)} e^{\\xi_{2}} + 24 \\, \\xi_{2} + 6\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{24 \\, \\xi_{2}^{4}} \\geq 0.92008662275004536$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi_{2}^{2} - 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 2 \\, \\xi_{2} + 1\\right)} e^{\\left(-\\xi_{2}\\right)}}{4 \\, \\xi_{2}^{3}} \\geq 0.87460114755231644$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(48 \\, \\xi_{2}^{2} + {\\left(19 \\, \\xi_{2}^{3} + 9 \\, \\xi_{2}^{2}\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 6 \\, {\\left(6 \\, \\xi_{2}^{2} + 3 \\, \\xi_{2} + 1\\right)} e^{\\xi_{2}} + 24 \\, \\xi_{2} + 6\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{24 \\, \\xi_{2}^{4}} \\geq 2.6692906048111306$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠6e067fae-e96d-4112-ba54-8296bf021da9is︠
%hide
%md 
---
#### No. 3.18
<a id="318"></a>
The combinatorial data is is given by $\Box= \text{conv} (\,(-3,0),(-2,1),(2,1),(3,0),(1,-2),(-1,-2)  \, )$ and  $\Phi_0(x,y) = \min \{0,\frac{-x-1}{2},-x\},\; \Phi_1(x,y)= \min \{ 0,y\}  ,\; \Phi_\infty(x,y) = \frac{x-y-1}{2}$.
︡0a6f85f4-304e-492b-a052-318ba4bbca06︡{"hide":"input"}︡{"md":"---\n#### No. 3.18\n<a id=\"318\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\,(-3,0),(-2,1),(2,1),(3,0),(1,-2),(-1,-2)  \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,\\frac{-x-1}{2},-x\\},\\; \\Phi_1(x,y)= \\min \\{ 0,y\\}  ,\\; \\Phi_\\infty(x,y) = \\frac{x-y-1}{2}$."}︡{"done":true}︡
︠ce8f0d43-2229-4de7-b5ed-b4a93649005as︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-3,0],[-2,1],[2,1],[3,0],[1,-2],[-1,-2]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[-1/2,-1/2,0],[0,-1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,0,1]]).transpose()
Phi3 = matrix(QQ,[[-1/2,1/2,-1/2]]).transpose();


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡a66e0d04-cdef-437e-95b5-250d4ade005f︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_WH5TJD.svg","show":true,"text":null,"uuid":"ab9e37bc-c9fd-410a-a92e-55f4783ec6d2"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -\\frac{1}{2} \\, x - \\frac{1}{2}, -x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, y\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\frac{1}{2} \\, x - \\frac{1}{2} \\, y - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠929435fe-9d04-4f38-9c72-94ae9a643617si︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix}-1 & 0\\ 0 & 1\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence, we have $\xi=(0,\xi_2)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡94bc5f79-c261-4d7f-bbe9-7de1db1cc3c7︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix}-1 & 0\\\\ 0 & 1\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence, we have $\\xi=(0,\\xi_2)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠a4042fa0-4aab-4dee-bf1e-e3a0a08774a3s︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,3,13])

# Analytic solution of the integral
F1(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([0,1,0])).simplify_full()

show(vector([F1,F2]))
︡20c9a2c0-b38a-42d2-b156-0b7261d06c6e︡{"html":"<div align='center'>$\\displaystyle \\xi_{2} \\ {\\mapsto}\\ \\left(0,\\,-\\frac{{\\left(4 \\, \\xi_{2}^{2} - {\\left(3 \\, \\xi_{2}^{3} + \\xi_{2}^{2} - 6 \\, \\xi_{2} - 6\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 24 \\, \\xi_{2} e^{\\left(2 \\, \\xi_{2}\\right)} - 6\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{2 \\, \\xi_{2}^{4}}\\right)$</div>"}︡{"done":true}︡
︠bc2f71d0-8d18-4205-8a62-7a67a7a2f86bis︠
%hide
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡6cabf1e7-c04b-499f-9f5c-14dbd390891f︡{"hide":"input"}︡{"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}︡{"done":true}︡
︠477acbbc-5fee-44c7-9d70-5f9aaa9dd0d5s︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
lower = 0.3796905
upper = 0.3797005

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡67d685a6-7b12-496a-9436-8c16f71d0068︡{"stdout":"Interval containing solution: [0.379684 .. 0.379708]\n"}︡{"done":true}︡
︠2a2f0045-6af8-4840-bda1-e3ed1d0fff48i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every (admissible) choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡3cc507d9-9aca-4473-9bcf-d543198b8075︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every (admissible) choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}
︠c338631f-618d-47d6-bbfc-1c895c77d504s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([0,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡ad3905ae-550a-4cdc-9c74-8fb48fea964e︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi_{2}^{2} + 2 \\, {\\left(\\xi_{2}^{3} - 3 \\, \\xi_{2} - 3\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 6 \\, {\\left(3 \\, \\xi_{2}^{2} + 2\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} - 6\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{6 \\, \\xi_{2}^{4}} \\geq 0.3694$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(12 \\, \\xi_{2}^{2} - {\\left(14 \\, \\xi_{2}^{3} + 15 \\, \\xi_{2}^{2} - 6\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 24 \\, {\\left(3 \\, \\xi_{2}^{2} - 3 \\, \\xi_{2} + 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 6 \\, \\xi_{2} - 30\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{12 \\, \\xi_{2}^{4}} \\geq 2.516$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(9 \\, \\xi_{2}^{2} - 2 \\, {\\left(\\xi_{2}^{3} - 3 \\, \\xi_{2} - 3\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 6 \\, {\\left(\\xi_{2}^{2} - 6 \\, \\xi_{2} + 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} - 12\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{6 \\, \\xi_{2}^{4}} \\geq 1.013$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(12 \\, \\xi_{2}^{2} + {\\left(14 \\, \\xi_{2}^{3} + 15 \\, \\xi_{2}^{2} - 6\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 12 \\, {\\left(2 \\, \\xi_{2}^{2} - 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} - 6 \\, \\xi_{2} - 6\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{12 \\, \\xi_{2}^{4}} \\geq 3.946$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠ae1183b5-9b1e-4282-8588-711bcd34f6f3i︠
%md 
---
#### No. 3.21
<a id="321"></a>
The combinatorial data is is given by $\Box= \text{conv} (\,(-2,1),(-3,3),(-1,3),(1,-2),(3,-3),(3,-1)  \, )$ and  $\Phi_0(x,y) = \min \{0,\frac{-x-1}{2}\},\; \Phi_1(x,y)= \min \{0,\frac{-y-1}{2}\} ,\; \Phi_\infty(x,y) = \min \{0,x+y\} $.
︡d55dd258-9eaf-4f72-93fe-126c7e612bbc︡{"done":true,"md":"---\n#### No. 3.21\n<a id=\"321\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\,(-2,1),(-3,3),(-1,3),(1,-2),(3,-3),(3,-1)  \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,\\frac{-x-1}{2}\\},\\; \\Phi_1(x,y)= \\min \\{0,\\frac{-y-1}{2}\\} ,\\; \\Phi_\\infty(x,y) = \\min \\{0,x+y\\} $."}
︠a9392dc1-cfaf-4387-9dca-c5d894510f38s︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-2,1],[-3,3],[-1,3],[1,-2],[3,-3],[3,-1]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[-1/2,-1/2,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[-1/2,0,-1/2]]).transpose()
Phi3 = matrix(QQ,[[0,0,0],[0,1,1]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡247c08c1-3695-44db-9be4-e88a0f008c6b︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_8F2Epv.svg","show":true,"text":null,"uuid":"0c0dce3b-62b3-49d3-8214-46b44ab311db"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -\\frac{1}{2} \\, x - \\frac{1}{2}\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -\\frac{1}{2} \\, y - \\frac{1}{2}\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, x + y\\right)$</div>"}︡{"done":true}︡
︠b0cfbbc1-1a7f-4871-be71-0528af9cca9dis︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix} 0 & 1\\ 1 & 0\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence we have $\xi=(\xi_1,\xi_1)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡e2692964-55cb-43d6-be5e-4f0c20bd0d14︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix} 0 & 1\\\\ 1 & 0\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence we have $\\xi=(\\xi_1,\\xi_1)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠fa62036a-24e7-4d2e-bc3f-a5ca3485219cs︠
#Choose a sufficiently general value for Lambda
Lambda = vector([1,3,13])

# Analytic solution of the integral
F1(xi_1) = intxexp(Polyhedra[0],Lambda,vector([xi_1,xi_1,0]),vector([1,-1,0])).simplify_full()
F2(xi_1) = intxexp(Polyhedra[0],Lambda,vector([xi_1,xi_1,0]),vector([1,1,0])).simplify_full()

show(vector([F1,F2]))
︡17119948-00ff-4e69-9b84-19fe9d325019︡{"html":"<div align='center'>$\\displaystyle \\xi_{1} \\ {\\mapsto}\\ \\left(0,\\,\\frac{{\\left(\\xi_{1}^{3} - \\xi_{1}^{2} + 4 \\, {\\left(\\xi_{1}^{2} - \\xi_{1}\\right)} e^{\\left(3 \\, \\xi_{1}\\right)} + 6 \\, {\\left(2 \\, \\xi_{1} - 1\\right)} e^{\\xi_{1}} - 2 \\, \\xi_{1} + 6\\right)} e^{\\left(-\\xi_{1}\\right)}}{\\xi_{1}^{4}}\\right)$</div>"}︡{"done":true}︡
︠b0cc04d6-9009-4330-aca5-7463bab4f811i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡09dee922-5fc6-40d3-bb1c-28e2af3bfb39︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠f25ad423-ddcb-4898-affb-fd6cac90565bs︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
find_root(F2,-10,10)
lower = -0.69631193
upper = -0.69621193

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡bc6fe2c8-c255-434e-a522-04960eeac047︡{"stdout":"-0.6962119314891655\n"}︡{"stdout":"Interval containing solution: [-0.696320 .. -0.696197]\n"}︡{"done":true}︡
︠43835ef4-9114-43f4-b722-db9aff6a280bis︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡1f44506d-fc8e-4c5a-bae5-8ca3e1b615e3︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠2b2ee542-3bd7-4fc2-9625-8fb6b779ec77s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([xi_2,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡9810319c-c846-4dfe-b418-cb13e7ef4343︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(\\xi_{2}^{3} - \\xi_{2}^{2} - 2 \\, {\\left(2 \\, \\xi_{2}^{2} - 4 \\, \\xi_{2} + 3\\right)} e^{\\xi_{2}} - 2 \\, \\xi_{2} + 6\\right)} e^{\\left(-\\xi_{2}\\right)}}{2 \\, \\xi_{2}^{4}} \\geq 0.6910$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(\\xi_{2}^{3} - \\xi_{2}^{2} - 2 \\, {\\left(2 \\, \\xi_{2}^{2} - 4 \\, \\xi_{2} + 3\\right)} e^{\\xi_{2}} - 2 \\, \\xi_{2} + 6\\right)} e^{\\left(-\\xi_{2}\\right)}}{2 \\, \\xi_{2}^{4}} \\geq 0.6910$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(5 \\, \\xi_{2}^{3} + 9 \\, \\xi_{2}^{2} + 6 \\, {\\left(8 \\, \\xi_{2}^{2} - 4 \\, \\xi_{2} + 1\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 24 \\, {\\left(6 \\, \\xi_{2}^{2} - 8 \\, \\xi_{2} + 5\\right)} e^{\\xi_{2}} - 66 \\, \\xi_{2} + 114\\right)} e^{\\left(-\\xi_{2}\\right)}}{24 \\, \\xi_{2}^{4}} \\geq 2.500$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(19 \\, \\xi_{2}^{3} - 33 \\, \\xi_{2}^{2} - 6 \\, {\\left(8 \\, \\xi_{2}^{2} - 4 \\, \\xi_{2} + 1\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 24 \\, {\\left(2 \\, \\xi_{2}^{2} - 1\\right)} e^{\\xi_{2}} + 18 \\, \\xi_{2} + 30\\right)} e^{\\left(-\\xi_{2}\\right)}}{24 \\, \\xi_{2}^{4}} \\geq 3.907$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠c6896162-5500-4d5f-bd02-4698cef78eabis︠
%hide
%md 
---
#### No. 3.22
<a id="322"></a>
The combinatorial data is is given by $\Box= \text{conv} (\,(-3,-1),(-3,0),(-1,1),(1,1),(3,0),(3,-1)  \, )$ and  $\Phi_0(x,y) = \min \{0,-x\},\; \Phi_1(x,y)= \min \{0,-y\} ,\; \Phi_\infty(x,y) = \frac{x-1}{2} $.
︡77bcb0a1-a250-48fa-b517-f4fe2fc8cbbf︡{"hide":"input"}︡{"md":"---\n#### No. 3.22\n<a id=\"322\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\,(-3,-1),(-3,0),(-1,1),(1,1),(3,0),(3,-1)  \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,-x\\},\\; \\Phi_1(x,y)= \\min \\{0,-y\\} ,\\; \\Phi_\\infty(x,y) = \\frac{x-1}{2} $."}︡{"done":true}︡
︠ee2e4fbc-ab7e-4b82-8e09-c0b01534fbfds︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-3,-1],[-3,0],[-1,1],[1,1],[3,0],[3,-1]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,-1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,0,-1]]).transpose()
Phi3 = matrix(QQ,[[-1/2,1/2,0]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡d9e5dbe3-b53f-472a-80d3-6976a87e9a77︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_RoME8z.svg","show":true,"text":null,"uuid":"69df9caf-8a1c-4d3c-8acc-33a3c83f45ea"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -y\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\frac{1}{2} \\, x - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠ade9ca2c-a426-4ba5-9796-37785a0fa7f3is︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix}-1 & 0\\ 0 & 1\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence, we have $\xi=(0,\xi_2)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡f9989650-cf19-489b-9bd6-f3657a0d7940︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix}-1 & 0\\\\ 0 & 1\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence, we have $\\xi=(0,\\xi_2)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠68ccfcc8-79fb-44a0-aef8-3f646416c35cs︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,5,13])

# Analytic solution of the integral
F1(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([0,1,0])).simplify_full()

show(vector([F1,F2]))
︡4813aad8-14cc-4a79-bbf8-9570f35a0e0c︡{"html":"<div align='center'>$\\displaystyle \\xi_{2} \\ {\\mapsto}\\ \\left(0,\\,\\frac{{\\left(9 \\, \\xi_{2}^{3} + 9 \\, \\xi_{2}^{2} + {\\left(\\xi_{2}^{3} + 3 \\, \\xi_{2}^{2} - 24\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 24 \\, {\\left(\\xi_{2} + 1\\right)} e^{\\xi_{2}}\\right)} e^{\\left(-\\xi_{2}\\right)}}{2 \\, \\xi_{2}^{4}}\\right)$</div>"}︡{"done":true}︡
︠6f8f9204-7876-45e1-9b52-e793c1d2f492i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡a95b19ad-6e36-44f4-9cb5-3b018e42eff7︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠a85e5d12-59f9-463e-b23a-9cc6c914c5e6s︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
find_root(F2,-10,10)
lower = 0.9147877
upper = 0.9147977

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡0095a9b9-d47d-4c44-99a6-da873752890a︡{"stdout":"0.9147877132620504\n"}︡{"stdout":"Interval containing solution: [0.914779 .. 0.914811]\n"}︡{"done":true}︡
︠8e5d01cd-5da8-43af-93b0-8dfacb26a159is︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡d270071c-cb8d-4491-bd24-435c549477a1︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠23a7e6bb-dda0-436d-849a-68b004bf3499s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([0,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡452356c1-0124-41f2-9aba-d8b0f1a062df︡{"html":"<div align='center'>$\\displaystyle \\frac{9 \\, \\xi_{2}^{2} + {\\left(\\xi_{2}^{3} + 3 \\, \\xi_{2}^{2} - 24\\right)} e^{\\xi_{2}} + 24 \\, \\xi_{2} + 24}{3 \\, \\xi_{2}^{4}} \\geq 0.8323$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(27 \\, \\xi_{2}^{3} + {\\left(\\xi_{2}^{3} - 24 \\, \\xi_{2} - 96\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 24 \\, {\\left(3 \\, \\xi_{2}^{2} + 5 \\, \\xi_{2} + 4\\right)} e^{\\xi_{2}}\\right)} e^{\\left(-\\xi_{2}\\right)}}{12 \\, \\xi_{2}^{4}} \\geq 2.164$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{9 \\, \\xi_{2}^{2} + {\\left(\\xi_{2}^{3} + 3 \\, \\xi_{2}^{2} - 24\\right)} e^{\\xi_{2}} + 24 \\, \\xi_{2} + 24}{6 \\, \\xi_{2}^{4}} \\geq 0.4161$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(27 \\, \\xi_{2}^{3} - {\\left(5 \\, \\xi_{2}^{3} + 18 \\, \\xi_{2}^{2} + 24 \\, \\xi_{2} - 48\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 6 \\, {\\left(3 \\, \\xi_{2}^{2} - 4 \\, \\xi_{2} - 8\\right)} e^{\\xi_{2}}\\right)} e^{\\left(-\\xi_{2}\\right)}}{12 \\, \\xi_{2}^{4}} \\geq 3.419$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠1e2e2917-808f-4e8f-ac74-2a7670d38c3cis︠
%hide
%md 
---
#### No. 3.23
<a id="323"></a>
The combinatorial data is is given by $\Box= \text{conv} (\, (-3,0),(-2,1),(1,1),(2,0),(2,-1), (0,3)\, )$ and  $\Phi_0(x,y) = \min \{0,-x\},\; \Phi_1(x,y)=  \min \{0,y\},\; \Phi_\infty(x,y) = \min \{-y, \frac{x-y-1}{2} \}$.
︡7b9ee062-342e-4a5b-bee9-f81a69140cb8︡{"hide":"input"}︡{"md":"---\n#### No. 3.23\n<a id=\"323\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\, (-3,0),(-2,1),(1,1),(2,0),(2,-1), (0,3)\\, )$ and  $\\Phi_0(x,y) = \\min \\{0,-x\\},\\; \\Phi_1(x,y)=  \\min \\{0,y\\},\\; \\Phi_\\infty(x,y) = \\min \\{-y, \\frac{x-y-1}{2} \\}$."}︡{"done":true}︡
︠0168cc1b-2656-4a51-8470-879134e2309as︠
# We are working with interval arithmetic with precision of 40 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(40)

# The base polytope
B=Polyhedron(vertices = [[-3,0],[-2,1],[1,1],[2,0],[2,-1],[0,-3]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,-1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,0,1]]).transpose()
Phi3 = matrix(QQ,[[-1/2,1/2,-1/2],[0,0,-1]]).transpose()

PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡4803e0cc-ed67-4285-a080-ca9f18715968︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_HPWJid.svg","show":true,"text":null,"uuid":"3b38fadf-b1c7-4429-85f9-b356583ab96d"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, y\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(\\frac{1}{2} \\, x - \\frac{1}{2} \\, y - \\frac{1}{2}, -y\\right)$</div>"}︡{"done":true}︡
︠6fe6cc9d-6b02-42b0-9079-e30b2d647e70si︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this we have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡15c08050-9125-4c25-b74b-fac3bcec3142︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this we have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠db2a27c5-d115-477c-93ec-36d92a8ce357s︠
#Choose a sufficiently general value for Lambda
Lambda = vector([3,13,19])

# The integral can be solved symbolically:
F1(xi_1,xi_2) = intxexp(Polyhedra[1],Lambda,vector([xi_1,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_1,xi_2) = intxexp(Polyhedra[1],Lambda,vector([xi_1,xi_2,0]),vector([0,1,0])).simplify_full()

show(F1)
show(F2)
︡80d15ae8-1d8f-432d-9b92-d4e44d153c96︡{"html":"<div align='center'>$\\displaystyle \\left( \\xi_{1}, \\xi_{2} \\right) \\ {\\mapsto} \\ -\\frac{{\\left(4 \\, \\xi_{1}^{4} e^{\\left(3 \\, \\xi_{1}\\right)} - {\\left(2 \\, {\\left({\\left(\\xi_{1} - 2\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} - {\\left(\\xi_{1} + 1\\right)} e^{\\xi_{1}} + 3 \\, e^{\\left(3 \\, \\xi_{1}\\right)}\\right)} \\xi_{2}^{4} + 4 \\, \\xi_{1}^{4} e^{\\left(3 \\, \\xi_{1}\\right)} + {\\left(2 \\, {\\left(\\xi_{1}^{2} - \\xi_{1}\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} + \\xi_{1} e^{\\left(3 \\, \\xi_{1}\\right)} + {\\left(2 \\, \\xi_{1}^{2} + \\xi_{1}\\right)} e^{\\xi_{1}}\\right)} \\xi_{2}^{3} - 2 \\, {\\left(6 \\, \\xi_{1}^{2} e^{\\left(3 \\, \\xi_{1}\\right)} + {\\left(\\xi_{1}^{3} - 4 \\, \\xi_{1}^{2}\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} - {\\left(\\xi_{1}^{3} + 2 \\, \\xi_{1}^{2}\\right)} e^{\\xi_{1}}\\right)} \\xi_{2}^{2} - {\\left(3 \\, \\xi_{1}^{3} e^{\\left(3 \\, \\xi_{1}\\right)} + 2 \\, {\\left(\\xi_{1}^{4} - 3 \\, \\xi_{1}^{3}\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} + {\\left(2 \\, \\xi_{1}^{4} + 3 \\, \\xi_{1}^{3}\\right)} e^{\\xi_{1}}\\right)} \\xi_{2}\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 2 \\, {\\left(2 \\, \\xi_{1}^{3} \\xi_{2}^{2} e^{\\left(5 \\, \\xi_{1}\\right)} + {\\left(3 \\, \\xi_{1}^{2} + {\\left(2 \\, \\xi_{1}^{2} - \\xi_{1}\\right)} e^{\\left(5 \\, \\xi_{1}\\right)} + \\xi_{1}\\right)} \\xi_{2}^{3} - {\\left(3 \\, \\xi_{1}^{4} + 3 \\, \\xi_{1}^{3} + {\\left(2 \\, \\xi_{1}^{4} - 3 \\, \\xi_{1}^{3}\\right)} e^{\\left(5 \\, \\xi_{1}\\right)}\\right)} \\xi_{2} - 2 \\, {\\left(\\xi_{1}^{5} - \\xi_{1}^{4}\\right)} e^{\\left(5 \\, \\xi_{1}\\right)}\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 4 \\, {\\left(\\xi_{1}^{3} \\xi_{2}^{2} e^{\\left(5 \\, \\xi_{1}\\right)} - {\\left(\\xi_{1}^{5} - \\xi_{1}^{4}\\right)} e^{\\left(5 \\, \\xi_{1}\\right)}\\right)} e^{\\left(2 \\, \\xi_{2}\\right)}\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{2 \\, {\\left(\\xi_{1}^{7} \\xi_{2} e^{\\left(3 \\, \\xi_{1}\\right)} - 2 \\, \\xi_{1}^{5} \\xi_{2}^{3} e^{\\left(3 \\, \\xi_{1}\\right)} + \\xi_{1}^{3} \\xi_{2}^{5} e^{\\left(3 \\, \\xi_{1}\\right)}\\right)}}$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\left( \\xi_{1}, \\xi_{2} \\right) \\ {\\mapsto} \\ -\\frac{{\\left(6 \\, \\xi_{1}^{4} \\xi_{2} e^{\\left(3 \\, \\xi_{1}\\right)} - 6 \\, \\xi_{1}^{2} \\xi_{2}^{3} e^{\\left(3 \\, \\xi_{1}\\right)} + 2 \\, \\xi_{1}^{4} e^{\\left(3 \\, \\xi_{1}\\right)} - 6 \\, \\xi_{1}^{2} \\xi_{2}^{2} e^{\\left(3 \\, \\xi_{1}\\right)} - {\\left(\\xi_{2}^{5} {\\left(2 \\, e^{\\left(4 \\, \\xi_{1}\\right)} - 3 \\, e^{\\left(3 \\, \\xi_{1}\\right)} + e^{\\xi_{1}}\\right)} - 2 \\, \\xi_{1}^{4} \\xi_{2} e^{\\left(3 \\, \\xi_{1}\\right)} + {\\left(2 \\, {\\left(\\xi_{1} - 1\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} - {\\left(\\xi_{1} - 3\\right)} e^{\\left(3 \\, \\xi_{1}\\right)} - {\\left(\\xi_{1} + 1\\right)} e^{\\xi_{1}}\\right)} \\xi_{2}^{4} + 2 \\, \\xi_{1}^{4} e^{\\left(3 \\, \\xi_{1}\\right)} - {\\left(2 \\, {\\left(\\xi_{1}^{2} + 2 \\, \\xi_{1}\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} - {\\left(5 \\, \\xi_{1}^{2} + 2 \\, \\xi_{1}\\right)} e^{\\left(3 \\, \\xi_{1}\\right)} + {\\left(\\xi_{1}^{2} - 2 \\, \\xi_{1}\\right)} e^{\\xi_{1}}\\right)} \\xi_{2}^{3} - {\\left(2 \\, {\\left(\\xi_{1}^{3} + \\xi_{1}^{2}\\right)} e^{\\left(4 \\, \\xi_{1}\\right)} - {\\left(\\xi_{1}^{3} - 3 \\, \\xi_{1}^{2}\\right)} e^{\\left(3 \\, \\xi_{1}\\right)} - {\\left(\\xi_{1}^{3} - \\xi_{1}^{2}\\right)} e^{\\xi_{1}}\\right)} \\xi_{2}^{2}\\right)} e^{\\left(4 \\, \\xi_{2}\\right)} + 2 \\, {\\left(\\xi_{1}^{4} e^{\\left(5 \\, \\xi_{1}\\right)} - 3 \\, \\xi_{1}^{2} \\xi_{2}^{2} e^{\\left(5 \\, \\xi_{1}\\right)} - 2 \\, {\\left(\\xi_{1} e^{\\left(5 \\, \\xi_{1}\\right)} - \\xi_{1}\\right)} \\xi_{2}^{3}\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 2 \\, {\\left(\\xi_{1}^{4} \\xi_{2} e^{\\left(5 \\, \\xi_{1}\\right)} - \\xi_{1}^{2} \\xi_{2}^{3} e^{\\left(5 \\, \\xi_{1}\\right)} + \\xi_{1}^{4} e^{\\left(5 \\, \\xi_{1}\\right)} - 3 \\, \\xi_{1}^{2} \\xi_{2}^{2} e^{\\left(5 \\, \\xi_{1}\\right)}\\right)} e^{\\left(2 \\, \\xi_{2}\\right)}\\right)} e^{\\left(-3 \\, \\xi_{2}\\right)}}{2 \\, {\\left(\\xi_{1}^{6} \\xi_{2}^{2} e^{\\left(3 \\, \\xi_{1}\\right)} - 2 \\, \\xi_{1}^{4} \\xi_{2}^{4} e^{\\left(3 \\, \\xi_{1}\\right)} + \\xi_{1}^{2} \\xi_{2}^{6} e^{\\left(3 \\, \\xi_{1}\\right)}\\right)}}$</div>"}︡{"done":true}︡
︠699a8260-78c4-4ef4-88c2-74ee1860ac6bis︠
%hide
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
We identify a small closed rectangle containing our estimate such that $ \nabla_n G > 0 $ for any outer normal of this rectangle, where $ G(\xi) = \int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du$. This and uniqueness guarantee our candidate lies within the rectangle.
︡a399e2a9-8df0-4b53-83e2-ce9b17e79001︡{"hide":"input"}︡{"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$.\nWe identify a small closed rectangle containing our estimate such that $ \\nabla_n G > 0 $ for any outer normal of this rectangle, where $ G(\\xi) = \\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du$. This and uniqueness guarantee our candidate lies within the rectangle."}︡{"done":true}︡
︠92ebd6cc-0c52-4b59-a0dc-a08091a8a759s︠

#numerically finding an estimate
from scipy import optimize

def FF(x):
    return [F1(x[0],x[1]),F2(x[0],x[1])]
sol=optimize.root(FF, [0.2, 0.5])
x0=sol.x

#We hope xi lies in the square with centre x0 and side length 2e, where:
e = 0.00001

xi_RIF=[RIF(x0[0]-e,x0[0]+e), RIF(x0[1]-e,x0[1]+e)]

#We use interval arithmetic to check positivity of $ \nabla_n G $ on the boundary of the square. We check line segments along the boundary of length 2e/N.
N=2300


if all([RIF(F1(x0[0]+e,RIF(x0[1]-e+i*(2*e/N),x0[1]-e+(i+1)*(2*e/N)))) > 0
        for i in range(N)]):
    if all([RIF(-F1(x0[0]-e,RIF(x0[1]-e+i*(2*e/N),x0[1]-e+(i+1)*(2*e/N)))) > 0
            for i in range(N)]):
        if all([RIF(F2(RIF(x0[0]-e+i*(2*e/N),x0[0]-e+(i+1)*(2*e/N)), x0[1]+e)) > 0 for i in range(N)]):
            if all([RIF(-F2(RIF(x0[0]-e+i*(2*e/N),x0[0]-e+(i+1)*(2*e/N)), x0[1]-e)) > 0 for i in range(N)]):
                print "There is a root in"
                print xi_RIF[0].str(style="brackets"),"x",xi_RIF[1].str(style="brackets")

︡4cd8abb0-4184-4230-b5af-4a8fec0e3536︡{"stdout":"There is a root in"}︡{"stdout":"\n[0.26616785810574 .. 0.26618785810616] x [0.67163062513918 .. 0.67165062513960]\n"}︡{"done":true}︡
︠2c3584e5-deda-4852-9517-5c33803dd802is︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every (admissible) choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡3019766c-51b6-41c6-b454-9650b9e0234c︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every (admissible) choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠09598c45-2cd0-45ee-a572-d5fb09884cea︠
#storage for the functions h_y and for the values h_y(xi) (as intervals)
Ih=[]

print "lower bounds for DF invariants"
for P in Polyhedra:
    h(xi_1,xi_2) = intxexp(P,Lambda,vector([xi_1,xi_2,0]),vector([0,0,1])).simplify_full()
    DF = RIF(h(*xi_RIF))
    Ih.append(DF)
    print(DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        print("negative upper bounds for DF invariant:")# print upper bound for destablising DF value
        if Ih[0] <= 0: print(Ih[0].upper())
        if Ih[1] <= 0: print(Ih[1].upper())
        if Ih[2] <= 0: print(Ih[2].upper())
        if Ih[3] <= 0: print(Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡c1034617-0c17-4ea1-ad39-619314509cd4︡{"stdout":"lower bounds for DF invariants\n"}︡{"stdout":"1.2766364162"}︡︡{"stdout":"\n1.8401675971"}︡{"stdout":"\n0.10047917120"}︡{"stdout":"\n3.4443270408"}︡{"stdout":"\n"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}
︠36ec290c-35ea-4a76-98aa-6fc1a640a5bfi︠
%md 
---
#### No. 3.24
<a id="324"></a>
The combinatorial data is is given by $\Box= \text{conv} (\, (-2,0),(-2,1),(1,1),(2,0),(2,-2),(0,-2) \, )$ and  $\Phi_0(x,y) = \min \{0,x\},\; \Phi_1(x,y)= \min \{0,y\}  ,\; \Phi_\infty(x,y) = \min \{0,-x-y\} $.
︡6b2235c1-06db-4e2f-a553-060e971dbd82︡{"done":true,"md":"---\n#### No. 3.24\n<a id=\"324\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\, (-2,0),(-2,1),(1,1),(2,0),(2,-2),(0,-2) \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,x\\},\\; \\Phi_1(x,y)= \\min \\{0,y\\}  ,\\; \\Phi_\\infty(x,y) = \\min \\{0,-x-y\\} $."}
︠163f811f-e859-4123-aea8-40ef0c94918bs︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-2,0],[-2,1],[1,1],[2,0],[2,-2],[0,-2]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,0,1]]).transpose()
Phi3 = matrix(QQ,[[0,0,0],[0,-1,-1]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡f9d34ac8-1f57-4ae9-a93a-823183b35f39︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_R2GtTn.svg","show":true,"text":null,"uuid":"3d45bd84-a5aa-4313-a282-c319fdfd4468"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, y\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -x - y\\right)$</div>"}︡{"done":true}︡
︠a97678c2-4d77-4b53-92eb-6e39a72ee77eis︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix}-1 & -1 \\ 1 & 0\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence, we have $\xi=(0,\xi_2)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡ab8db0ae-52b8-4abd-bdd4-fcc22396a3fa︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix}-1 & -1 \\\\ 1 & 0\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence, we have $\\xi=(0,\\xi_2)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠32099c02-cc64-498c-86ae-16a7bf391d5ds︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,3,13])

# Analytic solution of the integral
F1(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([0,1,0])).simplify_full()

show(vector([F1,F2]))
︡b7d4c36b-ee67-46d7-a0f5-d0b6fbfa2a8f︡{"html":"<div align='center'>$\\displaystyle \\xi_{2} \\ {\\mapsto}\\ \\left(-\\frac{{\\left({\\left(\\xi_{2}^{2} - 2\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 2 \\, \\xi_{2} + 4 \\, e^{\\left(2 \\, \\xi_{2}\\right)} - 2\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{\\xi_{2}^{3}},\\,\\frac{2 \\, {\\left({\\left(\\xi_{2}^{2} - 2\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} - 2 \\, \\xi_{2} + 4 \\, e^{\\left(2 \\, \\xi_{2}\\right)} - 2\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{\\xi_{2}^{3}}\\right)$</div>"}︡{"done":true}︡
︠10cd9b2a-36df-4ac4-ae8b-09e26e9d653ei︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡b0f4ba0d-3dc0-4104-8b77-74350d2d699e︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠801cc786-8ee8-4f45-ba76-a799af628e06s︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
lower = 0.434747
upper = 0.434757

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡00e2e7f5-0057-446f-a5fe-b3629e860b5b︡{"done":true}︡
︠efd6b222-1fdc-473c-b2ba-9da1c434e5d1i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every (admissible) choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡558c56f4-d406-4f34-b01f-44db2f029997︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every (admissible) choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}
︠96b126b3-685b-4eef-84e0-cbe08a930a62s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([0,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡20ed154d-f065-401a-98dd-d98ca64c97a3︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left({\\left(\\xi_{2} - 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + \\xi_{2} + 1\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{\\xi_{2}^{3}} \\geq 0.8784$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(12 \\, \\xi_{2}^{2} - {\\left(7 \\, \\xi_{2}^{3} + 3 \\, \\xi_{2}^{2} - 6 \\, \\xi_{2} + 6\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 12 \\, {\\left(2 \\, \\xi_{2}^{2} - 2 \\, \\xi_{2} + 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 12 \\, \\xi_{2} - 6\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{6 \\, \\xi_{2}^{4}} \\geq 1.288$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left({\\left(\\xi_{2} - 1\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + \\xi_{2} + 1\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{\\xi_{2}^{3}} \\geq 0.8784$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(12 \\, \\xi_{2}^{2} + {\\left(7 \\, \\xi_{2}^{3} + 3 \\, \\xi_{2}^{2} - 6 \\, \\xi_{2} + 6\\right)} e^{\\left(3 \\, \\xi_{2}\\right)} + 12 \\, \\xi_{2} - 12 \\, e^{\\left(2 \\, \\xi_{2}\\right)} + 6\\right)} e^{\\left(-2 \\, \\xi_{2}\\right)}}{6 \\, \\xi_{2}^{4}} \\geq 3.049$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠faf00806-87be-4724-a94e-c338c9a74d82i︠
%md 
---
#### No. 4.5*
<a id="45"></a>
The combinatorial data is is given by $\Box= \text{conv} (\,(-2,1),(-3,3),(-2,3),(1,-2),(3,-3),(3,-2) \, )$ and  $\Phi_0(x,y) = \min \{0,\frac{-x-1}{2}\},\; \Phi_1(x,y)= \min \{0,\frac{-y-1}{2}\}  ,\; \Phi_\infty(x,y) = \min \{0,x+y\} $.
︡ca9b5d6f-57d7-4b72-92da-69c4b75582c3︡{"done":true,"md":"---\n#### No. 4.5*\n<a id=\"45\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\,(-2,1),(-3,3),(-2,3),(1,-2),(3,-3),(3,-2) \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,\\frac{-x-1}{2}\\},\\; \\Phi_1(x,y)= \\min \\{0,\\frac{-y-1}{2}\\}  ,\\; \\Phi_\\infty(x,y) = \\min \\{0,x+y\\} $."}
︠dc395227-991a-49d3-b2e0-226e494ace82s︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-2,1],[-3,3],[-2,3],[1,-2],[3,-3],[3,-2]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[-1/2,-1/2,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[-1/2,0,-1/2]]).transpose()
Phi3 = matrix(QQ,[[0,0,0],[0,1,1]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡dcc5dac6-cfc4-4ad9-8b5d-4fe8d36084ee︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_oaAaLL.svg","show":true,"text":null,"uuid":"29a85e3f-02bf-4544-8bbe-789f87fee824"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -\\frac{1}{2} \\, x - \\frac{1}{2}\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -\\frac{1}{2} \\, y - \\frac{1}{2}\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, x + y\\right)$</div>"}︡{"done":true}︡
︠c1842cd0-8d4b-4bd0-82c8-b09a16c417f6is︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix}0 & 1\\ 1 & 0\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence, we have $\xi=(\xi_1,\xi_1)$ We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡6e11ca26-604b-4d51-87ae-4b87326b2758︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix}0 & 1\\\\ 1 & 0\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence, we have $\\xi=(\\xi_1,\\xi_1)$ We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠d6a078a3-ca60-4a0e-96c9-48c8a9feb15fs︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,3,13])

# Analytic solution of the integral
F1(xi_1) = intxexp(Polyhedra[0],Lambda,vector([xi_1,xi_1,0]),vector([1,-1,0])).simplify_full()
F2(xi_1) = intxexp(Polyhedra[0],Lambda,vector([xi_1,xi_1,0]),vector([1,1,0])).simplify_full()

show(vector([F1,F2]))
︡277aeed7-83c0-482f-8522-6229a7f3ba89︡{"html":"<div align='center'>$\\displaystyle \\xi_{1} \\ {\\mapsto}\\ \\left(0,\\,\\frac{{\\left(\\xi_{1}^{3} - \\xi_{1}^{2} + 2 \\, {\\left(\\xi_{1}^{3} - 2 \\, \\xi_{1}\\right)} e^{\\left(2 \\, \\xi_{1}\\right)} + 6 \\, {\\left(2 \\, \\xi_{1} - 1\\right)} e^{\\xi_{1}} - 2 \\, \\xi_{1} + 6\\right)} e^{\\left(-\\xi_{1}\\right)}}{\\xi_{1}^{4}}\\right)$</div>"}︡{"done":true}︡
︠092e56d1-a130-45e8-998e-21cac4cd74cai︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡2f9610f1-d7d6-47a3-b27f-65269ddf6f7c︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠8a8a40a5-195b-48fa-aa95-90e8b66f49b1s︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
find_root(F2,-10,10)
lower = -0.31044
upper = -0.31043

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡c3d301cd-d9cd-4010-a9e1-cd2041fa4d39︡{"stdout":"-0.31043117071394555\n"}︡{"stdout":"Interval containing solution: [-0.310441 .. -0.310424]\n"}︡{"done":true}︡
︠aeaaec55-68ed-48bc-8312-58babed6fb19is︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡33b2d8f1-7e6e-495a-8eac-ca3261ee91e5︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠c00ec5b5-5642-46a4-adf3-41791f49ac14s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([xi_2,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡fad13856-8bf9-47c4-97df-28cf7ea68c5a︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(\\xi_{2}^{3} - \\xi_{2}^{2} - 2 \\, {\\left(2 \\, \\xi_{2}^{2} - 4 \\, \\xi_{2} + 3\\right)} e^{\\xi_{2}} - 2 \\, \\xi_{2} + 6\\right)} e^{\\left(-\\xi_{2}\\right)}}{2 \\, \\xi_{2}^{4}} \\geq 0.5108$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(\\xi_{2}^{3} - \\xi_{2}^{2} - 2 \\, {\\left(2 \\, \\xi_{2}^{2} - 4 \\, \\xi_{2} + 3\\right)} e^{\\xi_{2}} - 2 \\, \\xi_{2} + 6\\right)} e^{\\left(-\\xi_{2}\\right)}}{2 \\, \\xi_{2}^{4}} \\geq 0.5108$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(5 \\, \\xi_{2}^{3} + 9 \\, \\xi_{2}^{2} + {\\left(37 \\, \\xi_{2}^{3} + 27 \\, \\xi_{2}^{2} - 18 \\, \\xi_{2} + 6\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} - 24 \\, {\\left(6 \\, \\xi_{2}^{2} - 8 \\, \\xi_{2} + 5\\right)} e^{\\xi_{2}} - 66 \\, \\xi_{2} + 114\\right)} e^{\\left(-\\xi_{2}\\right)}}{24 \\, \\xi_{2}^{4}} \\geq 2.318$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(19 \\, \\xi_{2}^{3} - 33 \\, \\xi_{2}^{2} - {\\left(37 \\, \\xi_{2}^{3} + 27 \\, \\xi_{2}^{2} - 18 \\, \\xi_{2} + 6\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 24 \\, {\\left(2 \\, \\xi_{2}^{2} - 1\\right)} e^{\\xi_{2}} + 18 \\, \\xi_{2} + 30\\right)} e^{\\left(-\\xi_{2}\\right)}}{24 \\, \\xi_{2}^{4}} \\geq 3.489$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠5736e476-d6cb-4954-81f4-4d08c982faeei︠
%md 
---
#### No. 4.8
<a id="48"></a>
The combinatorial data is is given by $\Box= \text{conv} (\,(-2,0),(-1,1),(1,1),(2,0),(2,-1),(-2,-1) \, )$ and  $\Phi_0(x,y) = \min \{0,x\},\; \Phi_1(x,y)= \min \{0,-x\} ,\; \Phi_\infty(x,y) = \min \{0,-y\}$.
︡c634e657-c64d-454c-9860-f97682c6011a︡{"done":true,"md":"---\n#### No. 4.8\n<a id=\"48\"></a>\nThe combinatorial data is is given by $\\Box= \\text{conv} (\\,(-2,0),(-1,1),(1,1),(2,0),(2,-1),(-2,-1) \\, )$ and  $\\Phi_0(x,y) = \\min \\{0,x\\},\\; \\Phi_1(x,y)= \\min \\{0,-x\\} ,\\; \\Phi_\\infty(x,y) = \\min \\{0,-y\\}$."}
︠6f090bd1-71f7-4a8c-86ad-923bd9511f67s︠
# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-2,0],[-1,1],[1,1],[2,0],[2,-1],[-2,-1]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,-1,0]]).transpose()
Phi3 = matrix(QQ,[[0,0,0],[0,0,-1]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)



print "The combinatorial data"


graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
︡d88a6487-1f47-4e8e-a465-5feda0e68050︡{"stdout":"The combinatorial data\n"}︡{"file":{"filename":"/projects/ae8e1663-e2ad-40b8-aec2-30faf4e6a54f/.sage/temp/compute5-us/19963/tmp_99xuY6.svg","show":true,"text":null,"uuid":"7ea83733-1477-4c3d-9a6c-dbe752b491c1"},"once":false}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -x\\right)$</div>"}︡{"stdout":" \n"}︡{"html":"<div align='center'>$\\displaystyle \\left( x, y \\right) \\ {\\mapsto} \\ \\min\\left(0, -y\\right)$</div>"}︡{"done":true}︡
︠1a50916b-fd9c-425c-94b5-a98ab2508574is︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}$
For this note, that $\left(\begin{smallmatrix}-1 & 0\\ 0 & 1\end{smallmatrix}\right)$ is a symmetry of the combinatorial data. Hence we have $\xi=(0,\xi_2)$. We have to analytically solve the integral $$\int_\Box \langle u,v \rangle \deg \bar \Phi (u) e^{\langle u, \xi \rangle } du = \int_{\Delta_0} \langle u', (v,0) \rangle \cdot e^{\langle u', (\xi,0) \rangle} du'$$ for $v$ varying over a basis of $N_\mathbb{R}$.
︡e8c59695-54a0-45f8-b476-c515b2b1c1ff︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}$\nFor this note, that $\\left(\\begin{smallmatrix}-1 & 0\\\\ 0 & 1\\end{smallmatrix}\\right)$ is a symmetry of the combinatorial data. Hence we have $\\xi=(0,\\xi_2)$. We have to analytically solve the integral $$\\int_\\Box \\langle u,v \\rangle \\deg \\bar \\Phi (u) e^{\\langle u, \\xi \\rangle } du = \\int_{\\Delta_0} \\langle u', (v,0) \\rangle \\cdot e^{\\langle u', (\\xi,0) \\rangle} du'$$ for $v$ varying over a basis of $N_\\mathbb{R}$.\n"}︡{"done":true}︡
︠fb350c7c-b44d-4f56-b53c-021e71b63691s︠
#Choose a sufficiently general value for lambda
Lambda = vector([1,5,13])


# Analytic solution of the integral
F1(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([0,1,0])).simplify_full()

show(vector([F1,F2]))
︡360d912a-d664-414e-8d13-7577be1a6bee︡{"html":"<div align='center'>$\\displaystyle \\xi_{2} \\ {\\mapsto}\\ \\left(0,\\,\\frac{{\\left(4 \\, \\xi_{2}^{3} + 4 \\, \\xi_{2}^{2} + {\\left(\\xi_{2}^{3} + \\xi_{2}^{2} - 2 \\, \\xi_{2} - 6\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 2 \\, {\\left(4 \\, \\xi_{2} + 3\\right)} e^{\\xi_{2}}\\right)} e^{\\left(-\\xi_{2}\\right)}}{\\xi_{2}^{4}}\\right)$</div>"}︡{"done":true}︡
︠606d588f-aaab-4d92-b349-ad4620282d1ci︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$.
︡a56ace71-0d2f-43b0-a1a7-d2caa2e2d1b5︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$."}
︠af429d18-be2c-4736-bce2-f8639dea86eds︠
# Choose upper and lower bound and
# hope that the exact solution lies in between
lower = 0.62430
upper = 0.62431

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
︡a4741cd8-c6e5-457e-9d96-d6b045403d06︡{"stdout":"Interval containing solution: [0.624298 .. 0.624314]\n"}︡{"done":true}︡
︠a0cd34c5-f72b-4bb5-be39-7f46e392bbb5is︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For this we first have to symbolically solve the integrals  
$$h_y(\xi) := \text{vol}(\Delta_y) \cdot \operatorname{DF}_\xi(\mathcal{X}_{y,0,1})=\int_{\Delta_y}  \langle u, (0,0,1) \, \rangle e^{\langle u, (\xi,0) \rangle }  \; du$$  
for every choice of $y \in \mathbb P^1$ and then plug in the estimate for $\xi$ into the resulting expression.
︡75acf183-879c-48c5-a3f4-9be364505c2d︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor this we first have to symbolically solve the integrals  \n$$h_y(\\xi) := \\text{vol}(\\Delta_y) \\cdot \\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})=\\int_{\\Delta_y}  \\langle u, (0,0,1) \\, \\rangle e^{\\langle u, (\\xi,0) \\rangle }  \\; du$$  \nfor every choice of $y \\in \\mathbb P^1$ and then plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠a443700b-dd99-4b44-9ae0-c29b52ad7748s︠
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([0,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡866fc1b4-be0d-4fb5-99dc-3020cc522858︡{"html":"<div align='center'>$\\displaystyle \\frac{4 \\, \\xi_{2}^{2} + {\\left(\\xi_{2}^{3} + \\xi_{2}^{2} - 2 \\, \\xi_{2} - 6\\right)} e^{\\xi_{2}} + 8 \\, \\xi_{2} + 6}{2 \\, \\xi_{2}^{4}} \\geq 0.6641$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{4 \\, \\xi_{2}^{2} + {\\left(\\xi_{2}^{3} + \\xi_{2}^{2} - 2 \\, \\xi_{2} - 6\\right)} e^{\\xi_{2}} + 8 \\, \\xi_{2} + 6}{2 \\, \\xi_{2}^{4}} \\geq 0.6641$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(4 \\, \\xi_{2}^{3} + {\\left(\\xi_{2}^{3} - 6 \\, \\xi_{2} - 12\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} + 6 \\, {\\left(2 \\, \\xi_{2}^{2} + 3 \\, \\xi_{2} + 2\\right)} e^{\\xi_{2}}\\right)} e^{\\left(-\\xi_{2}\\right)}}{3 \\, \\xi_{2}^{4}} \\geq 1.104$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(4 \\, \\xi_{2}^{3} - {\\left(2 \\, \\xi_{2}^{3} + 3 \\, \\xi_{2}^{2} - 6\\right)} e^{\\left(2 \\, \\xi_{2}\\right)} - 6 \\, {\\left(\\xi_{2} + 1\\right)} e^{\\xi_{2}}\\right)} e^{\\left(-\\xi_{2}\\right)}}{3 \\, \\xi_{2}^{4}} \\geq 2.442$</div>"}︡{"stdout":"Threefold is stable!\n"}︡{"done":true}︡
︠b0369181-8dbf-4818-bf9a-ed1a388c3e54︠









