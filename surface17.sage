#### No. 17: Degree $3$ / Singularity type $D_5$. 
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-2)/3,0)
Phi2(u)=min_symbolic((u-1)/2,0)
Phi3(u)=(-u-1)/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)

#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))

#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')

#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals

