load("threefold-tools.sage")

# We are working with interval arithmetic with precision of 40 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(40)

# The base polytope
B=Polyhedron(vertices = [[-3,0],[-2,1],[1,1],[2,0],[2,-1],[0,-3]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,-1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,0,1]]).transpose()
Phi3 = matrix(QQ,[[-1/2,1/2,-1/2],[0,0,-1]]).transpose()

PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)

print "The combinatorial data"

graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')
show(B)
show(B.vertices())

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
    
#Choose a sufficiently general value for Lambda
Lambda = vector([3,13,19])

# The integral can be solved symbolically:
F1(xi_1,xi_2) = intxexp(Polyhedra[1],Lambda,vector([xi_1,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_1,xi_2) = intxexp(Polyhedra[1],Lambda,vector([xi_1,xi_2,0]),vector([0,1,0])).simplify_full()

show(F1)
show(F2)


#numerically finding an estimate
from scipy import optimize

def FF(x):
    return [F1(x[0],x[1]),F2(x[0],x[1])]
sol=optimize.root(FF, [0.2, 0.5])
x0=sol.x

#We hope xi lies in the square with centre x0 and side length 2e, where:
e = 0.00001

xi_RIF=[RIF(x0[0]-e,x0[0]+e), RIF(x0[1]-e,x0[1]+e)]

#We use interval arithmetic to check positivity of $ \nabla_n G $ on the boundary of the square. We check line segments along the boundary of length 2e/N.
N=2300


if all([RIF(F1(x0[0]+e,RIF(x0[1]-e+i*(2*e/N),x0[1]-e+(i+1)*(2*e/N)))) > 0
        for i in range(N)]):
    if all([RIF(-F1(x0[0]-e,RIF(x0[1]-e+i*(2*e/N),x0[1]-e+(i+1)*(2*e/N)))) > 0
            for i in range(N)]):
        if all([RIF(F2(RIF(x0[0]-e+i*(2*e/N),x0[0]-e+(i+1)*(2*e/N)), x0[1]+e)) > 0 for i in range(N)]):
            if all([RIF(-F2(RIF(x0[0]-e+i*(2*e/N),x0[0]-e+(i+1)*(2*e/N)), x0[1]-e)) > 0 for i in range(N)]):
                print "There is a root in"
                print xi_RIF[0].str(style="brackets"),"x",xi_RIF[1].str(style="brackets")

#storage for the functions h_y and for the values h_y(xi) (as intervals)
Ih=[]

print "lower bounds for DF invariants"
for P in Polyhedra:
    h(xi_1,xi_2) = intxexp(P,Lambda,vector([xi_1,xi_2,0]),vector([0,0,1])).simplify_full()
    DF = RIF(h(*xi_RIF))
    Ih.append(DF)
    print(DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        print("negative upper bounds for DF invariant:")# print upper bound for destablising DF value
        if Ih[0] <= 0: print(Ih[0].upper())
        if Ih[1] <= 0: print(Ih[1].upper())
        if Ih[2] <= 0: print(Ih[2].upper())
        if Ih[3] <= 0: print(Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
