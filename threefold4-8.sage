load("threefold-tools.sage")

# We are working with interval arithmetic with precision of 60 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(16)

# The base polytope
B=Polyhedron(vertices = [[-2,0],[-1,1],[1,1],[2,0],[2,-1],[-2,-1]])

# 3 PL functions on B. If phi is the minimum of affine functions of the form F(x,y) = a + b*x + c*y we represent phi as
# the transpose of the matrix with rows [a,b,c].

Phi1 = matrix(QQ,[[0,0,0],[0,1,0]]).transpose()
Phi2 = matrix(QQ,[[0,0,0],[0,-1,0]]).transpose()
Phi3 = matrix(QQ,[[0,0,0],[0,0,-1]]).transpose()


PLfunctions = [Phi1,Phi2,Phi3]

# Form list of degeneration polyhedra
Polyhedra = degenerations(PLfunctions,B)

print "The combinatorial data"
graphic = B.plot().show(figsize=[3,3],title='Base Polytope B')
show(B)
show(B.vertices())

x,y = var('x,y')
r = vector([1,x,y])

for Phi in PLfunctions:
    if Phi.transpose().nrows() > 1:
        f(x,y) = min_symbolic(*list(r*Phi))
    else:
        f(x,y) = list(r*Phi)[0]
    print ' '
    show(f)
    
#Choose a sufficiently general value for lambda
Lambda = vector([1,5,13])


# Analytic solution of the integral
F1(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([1,0,0])).simplify_full()
F2(xi_2) = intxexp(Polyhedra[0],Lambda,vector([0,xi_2,0]),vector([0,1,0])).simplify_full()

show(vector([F1,F2]))

# Choose upper and lower bound and
# hope that the exact solution lies in between
lower = 0.62430
upper = 0.62431

#define a real value xi_RIF between xi_1 and xi_2
# representing the exact solution.
xi_RIF = RIF(lower,upper)

# Check whether Intermediate value theorem guarantees a zero beween
# lower and upper, i.e evaluate F2 at lower and upper using interval arithmetic.
if RIF(F2(lower)) < 0 and RIF(F2(upper)) > 0:
       print "Interval containing solution:", xi_RIF.str(style='brackets')
        
#storage for the functions h_y
H=[]
#storage for values h_y(xi) (as intervals)
Ih=[]

#Symbollically solving integrals using Barvinok method
for P in Polyhedra:
    h(xi_2) = intxexp(P,Lambda,vector([0,xi_2,0]),vector([0,0,1])).simplify_full()
    DF=RIF(h(xi_RIF))
    H.append(h); Ih.append(DF)
    show(h(xi_2) >= DF.lower())

if all([h > 0 for h in Ih]): 
    print("Threefold is stable!")
else:
    if any([h <= 0 for h in Ih]):
        print("Threefold is not stable!")

        # print upper bound for destablising DF value
        if Ih[0] <= 0: show(H[0](xi_2) <= Ih[0].upper())
        if Ih[1] <= 0: show(H[1](xi_2) <= Ih[1].upper())
        if Ih[2] <= 0: show(H[2](xi_2) <= Ih[2].upper())
        if Ih[3] <= 0: show(H[3](xi_2) <= Ih[3].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
