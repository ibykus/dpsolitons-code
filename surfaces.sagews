︠25b11441-a078-47d9-b11d-ecb32b380233si︠
%hide
%md
### Gorenstein del Pezzo Surfaces


1. Degree 1, Sing=$2D_4$
2. [Degree 1, Sing=$E_8$](#no2)
3. [Degree 1, Sing=$E_7A_1$](#no3)
4. [Degree 1, Sing=$E_6A_2$](#no4)
5. Degree 2, Sing=$2A_3A_1$
6. [Degree 2, Sing=$A_5A_2$](#no6)
7. [Degree 2, Sing=$D_6A_1$](#no7)
8. [Degree 2, Sing=$E_7$](#no8)
9. [Degree 2, Sing=$E_6$](#no9)
10. Degree 2, Sing=$2A_3$
11. [Degree 2, Sing=$D_5A_1$](#no11)
12. [Degree 2, Sing=$3A_1D_4$](#no12)
13. [Degree 3, Sing=$A_5A_1$](#no13)
14. [Degree 3, Sing=$E_6$](#no14)
15. [Degree 3, Sing=$A_4A_1$](#no15)
16. Degree 3, Sing=$2A_2A_1$
17. [Degree 3, Sing=$D_5$](#no17)
18. Degree 3, Sing=$2A_2$
19. [Degree 3, Sing=$D_4$](#no19)
20. [Degree 3, Sing=$A_32A_1$](#no20)
21. [Degree 4, Sing=$D_5$](#no21)
22. [Degree 4, Sing=$A_3A_1$](#no22)
23. [Degree 4, Sing=$D_4$](#no23)
24. [Degree 4, Sing=$A_4$](#no24)
25. Degree 4, Sing=$3A_1$
26. [Degree 4, Sing=$A_3$](#no26)
27. Degree 4, Sing=$2A_1$
28. [Degree 4, Sing=$A_2A_1$](#no28)
29. [Degree 5, Sing=$A_4$](#no29)
30. [Degree 5, Sing=$A_3$](#no30)
31. [Degree 5, Sing=$A_2$](#no31)
32. [Degree 5, Sing=$A_1$](#no32)
33. [Degree 6, Sing=$A_2$](#no33)
34. [Degree 6, Sing=$A_1$](#no34)

︡08131b12-c9d3-4fa8-9535-8f12d24d8945︡{"hide":"input"}︡{"md":"### Gorenstein del Pezzo Surfaces\n\n\n1. Degree 1, Sing=$2D_4$\n2. [Degree 1, Sing=$E_8$](#no2)\n3. [Degree 1, Sing=$E_7A_1$](#no3)\n4. [Degree 1, Sing=$E_6A_2$](#no4)\n5. Degree 2, Sing=$2A_3A_1$\n6. [Degree 2, Sing=$A_5A_2$](#no6)\n7. [Degree 2, Sing=$D_6A_1$](#no7)\n8. [Degree 2, Sing=$E_7$](#no8)\n9. [Degree 2, Sing=$E_6$](#no9)\n10. Degree 2, Sing=$2A_3$\n11. [Degree 2, Sing=$D_5A_1$](#no11)\n12. [Degree 2, Sing=$3A_1D_4$](#no12)\n13. [Degree 3, Sing=$A_5A_1$](#no13)\n14. [Degree 3, Sing=$E_6$](#no14)\n15. [Degree 3, Sing=$A_4A_1$](#no15)\n16. Degree 3, Sing=$2A_2A_1$\n17. [Degree 3, Sing=$D_5$](#no17)\n18. Degree 3, Sing=$2A_2$\n19. [Degree 3, Sing=$D_4$](#no19)\n20. [Degree 3, Sing=$A_32A_1$](#no20)\n21. [Degree 4, Sing=$D_5$](#no21)\n22. [Degree 4, Sing=$A_3A_1$](#no22)\n23. [Degree 4, Sing=$D_4$](#no23)\n24. [Degree 4, Sing=$A_4$](#no24)\n25. Degree 4, Sing=$3A_1$\n26. [Degree 4, Sing=$A_3$](#no26)\n27. Degree 4, Sing=$2A_1$\n28. [Degree 4, Sing=$A_2A_1$](#no28)\n29. [Degree 5, Sing=$A_4$](#no29)\n30. [Degree 5, Sing=$A_3$](#no30)\n31. [Degree 5, Sing=$A_2$](#no31)\n32. [Degree 5, Sing=$A_1$](#no32)\n33. [Degree 6, Sing=$A_2$](#no33)\n34. [Degree 6, Sing=$A_1$](#no34)"}︡{"done":true}︡
︠6cd7da68-7221-4135-b263-579ec5b435eeis︠
%hide
%md
---
#### No. 2: Degree $1$ / Singularity type $E_8$. <a id="no2"></a>
The combinatorial data is is given by $\Box=[-1,5]$ and $\Phi_0(u) = \min \{\frac{u-4}{5},0\},\; \Phi_\infty(u)= \frac{u-2}{3},\; \Phi_1(u) = -\frac{u+1}{2}$
︡0b90eac4-9086-40f5-92dc-27a5eec3d1f3︡{"hide":"input"}︡{"md":"---\n#### No. 2: Degree $1$ / Singularity type $E_8$. <a id=\"no2\"></a>\nThe combinatorial data is is given by $\\Box=[-1,5]$ and $\\Phi_0(u) = \\min \\{\\frac{u-4}{5},0\\},\\; \\Phi_\\infty(u)= \\frac{u-2}{3},\\; \\Phi_1(u) = -\\frac{u+1}{2}$"}︡{"done":true}︡
︠c05ce642-0cd5-4699-a9a4-092112b16152s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,5]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-4)/5,0)
Phi2(u)=1/3*u-2/3
Phi3(u)=-1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)

︡365f0451-5a6e-43c9-b3a6-b417c53525b1︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 5$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{5} \\, u - \\frac{4}{5}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠e921f142-1ef9-4304-899a-d933273b9bc5i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡2c648b44-5782-4f67-b618-aa8d02b348ac︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠7450debf-f34f-403e-8318-0b468e2b484as︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,*B)

show(F(xi))
︡604eef8b-1e5c-43c5-82dd-0081e27b4055︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(5 \\, \\xi - 2\\right)} e^{\\left(5 \\, \\xi\\right)}}{6 \\, \\xi^{3}} - \\frac{{\\left(12 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{30 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠d83f4ea0-2918-4885-97d1-71137cf2ac1di︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡c3a991ea-7e39-43f5-a386-3c045d6d84a1︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠fdeda8f3-aac2-45d7-bbbe-7acfcf9e9422s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡b7398701-dccb-47df-b396-3a0bebff76f9︡{"stdout":"Numerical solution: -1.9976195991\n"}︡{"stdout":"Interval containing solution: [-1.99761972 .. -1.99761947]\n"}︡{"done":true}︡
︠7bc23151-139a-4d6c-9afc-3d56c4b19f89si︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.

︡02c67614-bb11-4900-819d-2c27a15a87a2︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠9e470b4b-27f1-4219-b2b2-3dcfae5f896es︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals

︡4b39aea1-cc27-4b5b-b78f-50a818be1fcc︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(36 \\, {\\left(5 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)} + 11\\right)} e^{\\left(-\\xi\\right)}}{900 \\, \\xi^{3}} \\geq 0.01128570$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(12 \\, \\xi - 5\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(18 \\, {\\left(15 \\, \\xi - 8\\right)} e^{\\left(5 \\, \\xi\\right)} + 19\\right)} e^{\\left(-\\xi\\right)}}{900 \\, \\xi^{3}} \\geq 0.01949453$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(12 \\, \\xi - 5\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{{\\left(12 \\, {\\left(25 \\, \\xi - 13\\right)} e^{\\left(5 \\, \\xi\\right)} + 30 \\, \\xi + 31\\right)} e^{\\left(-\\xi\\right)}}{900 \\, \\xi^{3}} \\geq 0.02975460$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(6 \\, {\\left(25 \\, \\xi - 4\\right)} e^{\\left(5 \\, \\xi\\right)} - 30 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{900 \\, \\xi^{3}} \\geq 0.06053484$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠c537d312-b684-467c-9ca6-b553ee53b897i︠
%md 
------------------
#### No. 3: Degree $1$ / Singularity type $E_7A_1$. <a id="no3"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{\frac{u-2}{3},0\},\; \Phi_\infty(u)= \frac{u-3}{4},\; \Phi_1(u) = \frac{-u-1}{2}$
︡9031103d-9e67-4bbb-a785-3bee4ea1d517︡{"done":true,"md":"------------------\n#### No. 3: Degree $1$ / Singularity type $E_7A_1$. <a id=\"no3\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{\\frac{u-2}{3},0\\},\\; \\Phi_\\infty(u)= \\frac{u-3}{4},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠048762b2-b6c7-4db0-bc25-a5c47d059df1s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-2)/3,0)
Phi2(u)=1/4*u-3/4
Phi3(u)=-1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)


︡8bddc9a1-0a11-4a10-bc3d-e467855b33e2︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{3} \\, u - \\frac{2}{3}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{4} \\, u - \\frac{3}{4}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠18d0d12d-4e96-49c6-8ccd-e51b76118f15i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡ad2b1359-4623-49e7-9211-9fd7b9c0e409︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠b544de5b-b198-4457-8c94-3d7f999b4f25s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡0e2412a6-2ff8-4b50-9e43-3432e1fa91d2︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(8 \\, {\\left(\\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{12 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠563ff947-1561-411c-917f-f5c8aa34c144i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡ebbc4210-6f84-482d-8ddc-d5567a7377a9︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠55c5e070-9b2b-454b-9a14-ad3cb5127593s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡4f8d1bfe-0b10-4d75-8a2d-2083865ed903︡{"stdout":"Numerical solution: -1.9402428939\n"}︡{"stdout":"Interval containing solution: [-1.94024301 .. -1.94024276]\n"}︡{"done":true}︡
︠3848a421-c90b-444d-800a-41dbaa3074a7is︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡bf1665dd-f211-4695-b246-ebbd32be1f0a︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠04b0db85-5a0e-4756-96f4-63c81fab58fas︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡884787cb-7310-4f9a-972e-152f7b6f0458︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} - \\frac{{\\left(16 \\, {\\left(3 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + 7\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.04440443$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} - \\frac{{\\left(8 \\, {\\left(3 \\, \\xi - 4\\right)} e^{\\left(3 \\, \\xi\\right)} + 5\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.03181965$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} + \\frac{{\\left(4 \\, {\\left(9 \\, \\xi - 10\\right)} e^{\\left(3 \\, \\xi\\right)} + 12 \\, \\xi + 13\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.06993168$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} - \\frac{{\\left(4 \\, {\\left(9 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 12 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.1461557$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠e0248517-b402-40a3-9d6b-5cec42b33865i︠
%md 
------------------
#### No. 4: Degree $2$ / Singularity type $E_6A_2$. <a id="no4"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{\frac{u-1}{2},0\},\; \Phi_\infty(u)= \frac{u-2}{3},\; \Phi_1(u) = \frac{-2u-2}{3}$
︡2661d438-77d2-4245-a695-e944c82da752︡{"done":true,"md":"------------------\n#### No. 4: Degree $2$ / Singularity type $E_6A_2$. <a id=\"no4\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_\\infty(u)= \\frac{u-2}{3},\\; \\Phi_1(u) = \\frac{-2u-2}{3}$"}
︠e7a2ee6a-8481-4adf-b927-4687748d498bs︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-1)/2,0)
Phi2(u)=1/3*u-2/3
Phi3(u)=-2/3*u-2/3

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)


︡efeef0d5-46d4-4001-9e9e-07cc4506aa20︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{2}{3} \\, u - \\frac{2}{3}$</div>"}︡{"done":true}︡
︠b2a33c82-5599-49e3-a76d-36d56a5e2975i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡c720bd01-5063-4dda-a906-975de45cae1f︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠4fa785b7-8693-4797-be4e-851556a62b21s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡94b0b019-7ce4-4480-b9b6-249a4d3c3784︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)}}{3 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠4dcc396d-35f5-4e4f-899e-1d6139b385dci︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡afbb6fc7-1c8e-4886-b141-885f13d57b84︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠24b8e7ff-6c29-4928-8e34-6b753b77d242s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡1a2ee1c8-064b-422d-bec5-af304232aed8︡{"stdout":"Numerical solution: -1.69131741884\n"}︡{"stdout":"Interval containing solution: [-1.69131753 .. -1.69131729]\n"}︡{"done":true}︡
︠8c2d7dcb-f923-4652-a06c-a93d41b66fdfis︠
%hide
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡1154624e-0668-4799-9bb4-55eb4a571d48︡{"hide":"input"}︡{"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}︡{"done":true}︡
︠5b802cea-cc70-494c-99a0-d949229d96fds︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡c7f648c8-46d1-4859-ae2b-3f1585c0c2e6︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{{\\left(9 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 5\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1187884$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)}}{3 \\, \\xi^{3}} - \\frac{{\\left({\\left(2 \\, \\xi - 5\\right)} e^{\\left(2 \\, \\xi\\right)} + 1\\right)} e^{\\left(-\\xi\\right)}}{12 \\, \\xi^{3}} \\geq 0.07315991$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)}}{3 \\, \\xi^{3}} + \\frac{{\\left({\\left(4 \\, \\xi - 7\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, \\xi + 3\\right)} e^{\\left(-\\xi\\right)}}{12 \\, \\xi^{3}} \\geq 0.07315988$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(4 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 6 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.2651082$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠333e3f65-68fa-45d8-84c1-6b83fb0aacb3i︠
%md 
------------------
#### No. 6: Degree $2$ / Singularity type $A_5A_2$. <a id="no6"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{-u,0\},\; \Phi_\infty(u)= \frac{u-2}{3},\; \Phi_1(u) = \frac{u-2}{3}$
︡d279829a-8af8-4e24-9821-872b7f08a01b︡{"done":true,"md":"------------------\n#### No. 6: Degree $2$ / Singularity type $A_5A_2$. <a id=\"no6\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{-u,0\\},\\; \\Phi_\\infty(u)= \\frac{u-2}{3},\\; \\Phi_1(u) = \\frac{u-2}{3}$"}
︠5292b407-c63b-4bd3-abd7-bf64e4d44503s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(-u,0)
Phi2(u)=1/3*u-2/3
Phi3(u)=1/3*u-2/3

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)


︡63ed6985-2f96-4bf7-bf25-38fb993b3bde︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-u, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"done":true}︡
︠53aae284-fa67-443f-ae4e-806aac4a2295i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡c5ccc61b-0510-4ccb-b4c0-294290fc8d7c︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠3672c00d-101c-4522-9ec1-8fedaff9d469s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡b07fd5ae-613b-4355-94ac-6f0b3c48a66f︡{"html":"<div align='center'>$\\displaystyle -\\frac{2 \\, {\\left(\\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{3 \\, \\xi^{3}} + \\frac{2 \\, {\\left({\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 3\\right)}}{3 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠2e6de85c-7f3c-46ba-b19a-18b7ecc1f049i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡464a9cb8-fef8-4ae9-beb6-f3cabbbcd6f8︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠655e11e3-75bc-4d69-8a7e-4bbd223cb0b5s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡09db5039-8a0a-4551-89ae-9d39b2cdf641︡{"stdout":"Numerical solution: -0.97052220918\n"}︡{"stdout":"Interval containing solution: [-0.970522315 .. -0.970522105]\n"}︡{"done":true}︡
︠c349cfc9-b783-47f1-a6b4-5e327083b884i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡f4ec2cdb-8c19-41c8-94fa-a354956d2ea7︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠9cd1093a-981b-4fd4-90f2-4a7d37e9de75s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡e5f73db6-372a-4d89-ac4a-35d3c4f9adc2︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{{\\left(3 \\, \\xi - 5\\right)} e^{\\left(2 \\, \\xi\\right)} + 9 \\, \\xi + 9}{9 \\, \\xi^{3}} \\geq 0.4790611$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1}{3 \\, \\xi^{3}} \\geq 0.09239867$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1}{3 \\, \\xi^{3}} \\geq 0.09239867$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} + \\frac{{\\left(3 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 3 \\, \\xi - 3}{9 \\, \\xi^{3}} \\geq 0.6638587$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠641f04db-9192-4076-9d64-81086e225feai︠
%md 
------------------
#### No. 7: Degree $2$ / Singularity type $D_6A_1$. <a id="no7"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{\frac{u-1}{2},0\},\; \Phi_\infty(u)= \frac{u-3}{4},\; \Phi_1(u) = \frac{-u-1}{2}$
︡598eabe0-3c2d-474b-beb7-e17d3b78580e︡{"done":true,"md":"------------------\n#### No. 7: Degree $2$ / Singularity type $D_6A_1$. <a id=\"no7\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_\\infty(u)= \\frac{u-3}{4},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠b700922f-4c8a-4b03-8732-60bd5cb2a913s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-1)/2,0)
Phi2(u)=1/4*u-3/4
Phi3(u)=-1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡1e332667-a164-4b32-8c10-5f1ed35654cc︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{4} \\, u - \\frac{3}{4}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠b579655d-97ad-4e1a-b98e-0d12470bb14bi︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡85416c33-5a20-4bdf-90ed-ee52ab3b2499︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠e7bdbdf2-b43a-4c63-b288-1378d97e84afs︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡18a24282-9810-4b53-9533-928fdd741eae︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠caf5c089-a05a-42c6-a66f-5c4c60e5aa3fi︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡17ca1bbc-124f-482d-aa25-9c31d4ddd923︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠46e6cfba-6291-434c-8383-c16dada3331ds︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡2300093e-9c6e-410c-a6b4-f9dcc548e4ae︡{"stdout":"Numerical solution: -1.79675598472\n"}︡{"stdout":"Interval containing solution: [-1.79675609 .. -1.79675588]\n"}︡{"done":true}︡
︠112a3e8c-21c8-453c-a530-f877408f8546i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡ebe4a5a7-4b54-4076-8ea8-0e485161a45e︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠f12ba782-4126-4735-9893-a257c8211426s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡8beb476d-52cb-484a-a62d-8e52c3d5773e︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} - \\frac{{\\left(4 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 3\\right)} e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} \\geq 0.1624897$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} + \\frac{{\\left(4 \\, e^{\\left(2 \\, \\xi\\right)} - 1\\right)} e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} \\geq 0.05832627$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} + \\frac{{\\left(4 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 4 \\, \\xi + 5\\right)} e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} \\geq 0.1687342$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{16 \\, \\xi^{3}} - \\frac{{\\left(4 \\, \\xi e^{\\left(2 \\, \\xi\\right)} - 4 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} \\geq 0.3895503$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠be0aa7a9-0391-4313-90c9-b3d152e17f68i︠
%md 
------------------
#### No. 8: Degree $2$ / Singularity type $E_7$. <a id="no8"></a>
The combinatorial data is is given by $\Box=[-1,5]$ and $\Phi_0(u) = \min \{\frac{u-3}{4},0\},\; \Phi_\infty(u)= \frac{u-2}{3},\; \Phi_1(u) = \frac{-u-1}{2}$
︡929934d6-2f4e-4cda-aece-d4ecc32aa346︡{"done":true,"md":"------------------\n#### No. 8: Degree $2$ / Singularity type $E_7$. <a id=\"no8\"></a>\nThe combinatorial data is is given by $\\Box=[-1,5]$ and $\\Phi_0(u) = \\min \\{\\frac{u-3}{4},0\\},\\; \\Phi_\\infty(u)= \\frac{u-2}{3},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠767ab4ba-297a-47fb-876a-9b59c34914aas︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,5]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-3)/4,0)
Phi2(u)=1/3*(u-2)
Phi3(u)=1/2*(-u-1)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡ddebdcca-acbc-47fd-86b3-1e89b7e544f4︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 5$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{4} \\, u - \\frac{3}{4}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠eb7a47bd-612a-414b-a5a5-f1b71e17d5a6i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡5357b86a-bf34-473a-950e-2a98bf001a66︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠f2f5fe0e-5001-41e5-8d83-b999868e7012s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡c9a711b1-9285-4897-8e3f-55d092d4188e︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(5 \\, \\xi - 2\\right)} e^{\\left(5 \\, \\xi\\right)}}{6 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(4 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{12 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠3c21bd80-7f17-449f-a15c-ca7a4acf5043i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡b7103137-1076-406f-b134-ed174d314092︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠69dea6af-30a0-4d22-abfd-34b3520abfb2s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡9c64d680-4d62-4018-9d5c-750cfaf626f2︡{"stdout":"Numerical solution: -1.99186220915\n"}︡{"stdout":"Interval containing solution: [-1.99186233 .. -1.99186208]\n"}︡{"done":true}︡
︠ffdb2ffc-6404-4a72-a65b-5ba38181bf0ai︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡a2186c5b-cb2d-427a-8e10-2340a03f2b34︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠6242521f-7c21-4069-b292-13dd488b58f1s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡f92b4792-a49c-424d-b38f-06c8cf052aa4︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(9 \\, {\\left(4 \\, \\xi - 1\\right)} e^{\\left(4 \\, \\xi\\right)} + 5\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.03202411$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(12 \\, \\xi - 5\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(9 \\, {\\left(4 \\, \\xi - 3\\right)} e^{\\left(4 \\, \\xi\\right)} + 7\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.04486743$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(12 \\, \\xi - 5\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{{\\left(3 \\, {\\left(16 \\, \\xi - 11\\right)} e^{\\left(4 \\, \\xi\\right)} + 12 \\, \\xi + 13\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.07049793$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(8 \\, \\xi - 1\\right)} e^{\\left(4 \\, \\xi\\right)} - 12 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{144 \\, \\xi^{3}} \\geq 0.1473894$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠bda9ebd5-cebd-4eb8-9b70-49365486085ci︠
%md 
------------------
#### No. 9: Degree $2$ / Singularity type $E_6$. <a id="no9"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{\frac{u-2}{3},0\},\; \Phi_\infty(u)= \min \{\frac{u-2}{3},0\},\; \Phi_1(u) = \frac{-u-1}{2}$
︡f672be90-1b28-415a-a06d-3e47184c86d8︡{"done":true,"md":"------------------\n#### No. 9: Degree $2$ / Singularity type $E_6$. <a id=\"no9\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{\\frac{u-2}{3},0\\},\\; \\Phi_\\infty(u)= \\min \\{\\frac{u-2}{3},0\\},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠00317771-2a7f-497f-bb39-69fd5354a2f9s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-2)/3,0)
Phi2(u)=min_symbolic((u-2)/3,0)
Phi3(u)=1/2*(-u-1)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)

︡0c9f435e-599c-4f3c-8f41-2ba4e56f6210︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{3} \\, u - \\frac{2}{3}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{3} \\, u - \\frac{2}{3}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠50fc0478-7e60-4b90-a21d-288d6b746313i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡c51461dc-484f-4351-b576-1b3c45fc7c2f︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠c93be603-3a58-47d7-89f7-b002dbc14e64s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡9f1ca87a-13cd-464d-b2ec-05e8bb8b6589︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{2 \\, \\xi^{3}} - \\frac{{\\left(8 \\, {\\left(\\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠39f3e583-8f62-4c39-b800-fa623bd6b51asi︠
%hide
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡d99c6943-76ce-4351-9112-79ae26c88699︡{"hide":"input"}︡{"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}︡{"done":true}︡
︠86b8eeaf-d28e-43dc-aed0-aa8e472ea4acs︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡14885daa-8d72-4b28-800c-c40d0dee9665︡{"stdout":"Numerical solution: -1.9402428939\n"}︡{"stdout":"Interval containing solution: [-1.94024301 .. -1.94024276]\n"}︡{"done":true}︡
︠59a819fb-ba1d-4c9f-91f4-8bd60feca7dci︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡14b0ba95-7612-4f5c-9f69-8ba7c0ed29a5︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠45080f68-cf3d-49f1-99ed-9c83190be63es︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡50dbd636-4aaf-4ac8-86be-46609ec028b4︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} + 1\\right)} e^{\\left(-\\xi\\right)}}{12 \\, \\xi^{3}} \\geq 0.07622408$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} + 1\\right)} e^{\\left(-\\xi\\right)}}{12 \\, \\xi^{3}} \\geq 0.07622408$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(8 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} + 6 \\, \\xi + 7\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1272785$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(4 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 6 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.2797267$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠7a393b6d-b740-4f92-8516-189dbef1380ai︠
%md 
------------------
#### No. 11: Degree $2$ / Singularity type $D_5A_1$. <a id="no11"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{\frac{u-1}{2},0\},\; \Phi_\infty(u)= \min \{\frac{u-1}{2},0\},\; \Phi_1(u) = \frac{-2u-2}{3}$
︡6d46e90b-87d2-4077-ac1d-17ad0d7e843b︡{"done":true,"md":"------------------\n#### No. 11: Degree $2$ / Singularity type $D_5A_1$. <a id=\"no11\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_\\infty(u)= \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_1(u) = \\frac{-2u-2}{3}$"}
︠77d1722c-61e4-4cf3-8a10-23b63aed9c05s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-1)/2,0)
Phi2(u)=min_symbolic((u-1)/2,0)
Phi3(u)=1/3*(-2*u-2)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡42053bcc-1b05-4870-8767-d4c9bfe825a0︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{2}{3} \\, u - \\frac{2}{3}$</div>"}︡{"done":true}︡
︠f6704d49-5ba0-4d3a-b056-1adf799ea63di︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡cdb06e10-b0c7-446b-a82e-778307bcfc71︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠d11ebccf-d19e-4b47-8d6c-42525368a293s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡22653d89-7c8c-40ab-afbe-c97ee39e184f︡{"html":"<div align='center'>$\\displaystyle \\frac{4 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)}}{3 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{3 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠fda49567-f038-44a2-8087-212ea7fb7e45i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡d3c01ec9-deb8-46b3-946b-451418928858︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠684115e7-9566-4e13-b93c-e8b2a1483bc5s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.00001
xi_2=xi_1+0.00002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡7ec3532f-c91f-45fd-b3d4-cb3319625138︡{"stdout":"Numerical solution: -1.69131741884\n"}︡{"stdout":"Interval containing solution: [-1.69132743 .. -1.69130739]\n"}︡{"done":true}︡
︠0cf71f68-737f-46a7-80f3-2507d4c0ceddi︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡5db1fe3b-bac7-460f-8558-d321dbaaeeac︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠4228d0fd-9b82-41ad-8320-a9a045e053b7s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡39508d84-4c03-46ea-ac45-caf3a1d4a8aa︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{2 \\, {\\left(3 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 1\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq 0.1919413$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{2 \\, {\\left(3 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 1\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq 0.1919413$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} + \\frac{{\\left(9 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 3 \\, \\xi + 5\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq 0.1006816$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 3 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq 0.4845700$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠4e06ff10-e327-4b12-8af7-0e81460d3df1i︠
%md 
------------------
#### No. 12: Degree $2$ / Singularity type $3A_1D_4$. <a id="no12"></a>
The combinatorial data is is given by $\Box=[-1,1]$ and $\Phi_0(u) = \frac{u-1}{2},\; \Phi_\infty(u)= \frac{u-1}{2},\; \Phi_1(u) = \frac{-u-1}{2}$
︡4b59062a-9fac-4039-af35-99145bc7be46︡{"done":true,"md":"------------------\n#### No. 12: Degree $2$ / Singularity type $3A_1D_4$. <a id=\"no12\"></a>\nThe combinatorial data is is given by $\\Box=[-1,1]$ and $\\Phi_0(u) = \\frac{u-1}{2},\\; \\Phi_\\infty(u)= \\frac{u-1}{2},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠1dcfeb52-2147-45d8-b6d6-a8fbe28c6a82s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=1/2*(u-1)
Phi2(u)=1/2*(u-1)
Phi3(u)=1/2*(-u-1)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡ebe56a03-6706-46db-8275-8b4c6948103f︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠ba9d9fef-f82a-4ad5-853a-70b6da10d2f7i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡75074abb-271b-4b32-b69b-81bdaba8c8ed︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠69bb0a12-ea85-49ce-91a4-d3e9e2109d37s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡cde46fb6-a331-4eff-bbcc-6abde91b11da︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(12 \\, \\xi^{2} - 7 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{2 \\, \\xi^{3}} - \\frac{{\\left(\\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠63e0c47a-52d6-470c-afd7-ea7a518df824i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡27cac9b5-b28d-4a28-80ae-b3a4b0d525ff︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠3a36e9a6-49e9-4a9f-b15e-17f0d700de82s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡755148d2-7cc7-45cf-aac7-50bd582e123b︡{"stdout":"Numerical solution: -1.97691850478\n"}︡{"stdout":"Interval containing solution: [-1.97691861 .. -1.97691839]\n"}︡{"done":true}︡
︠c022e9b7-29dd-4c49-9a68-0c9e51150a9fi︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡55b47db1-34fd-4111-a10e-ede8e89b04b6︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠e1e84bc5-e6ea-40b7-b2ed-f7fcf9f2a913s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡f819021c-7dc6-4d4d-a823-0b1240e28da2︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(8 \\, \\xi^{2} - 4 \\, \\xi + 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.2301820$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(8 \\, \\xi^{2} - 4 \\, \\xi + 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.2301820$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(16 \\, \\xi^{2} - 10 \\, \\xi + 3\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi + 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.2301820$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi + 1\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.6905461$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠bf93b227-25fa-4019-88c6-c837be50f68di︠
%md 
---
#### No. 13: Degree $3$ / Singularity type $A_5A_1$. <a id="no13"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{-u,0\},\; \Phi_\infty(u)= \frac{u-3}{4},\; \Phi_1(u) = \frac{u-1}{2}$
︡5d054cca-355e-4a8a-a6ba-4de9b493c96c︡{"done":true,"md":"---\n#### No. 13: Degree $3$ / Singularity type $A_5A_1$. <a id=\"no13\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{-u,0\\},\\; \\Phi_\\infty(u)= \\frac{u-3}{4},\\; \\Phi_1(u) = \\frac{u-1}{2}$"}
︠ed04c9f6-e84d-4d8b-8879-b1010652e1e6s︠
# We are working with interval arithmetic with precision of 16 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(11)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(-u,0)
Phi2(u)=1/4*u-3/4
Phi3(u)=1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡5caffb4b-f2d6-4b97-a315-e519851ac250︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-u, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{4} \\, u - \\frac{3}{4}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠e6757ec2-cdb7-4635-976f-9d50de74a746i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡e8c528ea-a88f-4528-8972-ed16ca587eaf︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠1a1f65cc-f54d-44ac-887f-903c3b5cc11ds︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])

show(F(xi))
︡7b2f27e1-750b-423d-9a1d-78e7f8fc4862︡{"html":"<div align='center'>$\\displaystyle -\\frac{3 \\, {\\left(\\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} + 8}{4 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠ecdfdd3d-c753-49d5-b3f1-c55782bd3c38i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡ca6fd50e-306d-4b75-b0e6-a37283dfb44b︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠71d7993a-58e5-4f04-aea0-742e4f7a3f5cs︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.001
xi_2=sol+0.001

xi_1=-1.247
xi_2=-1.246

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0: 
    print "Interval containing solution:", xi0.str(style='brackets')
︡70eee53f-6113-4eba-b729-03a1394b14fd︡{"stdout":"Numerical solution: -1.24607304295\n"}︡{"stdout":"Interval containing solution: [-1.2471 .. -1.2451]\n"}︡{"done":true}︡
︠da7cbebc-288d-4ec1-a3da-6557833e5f13s︠

︡41050f6a-6b23-4fdd-a88c-1762142932c0︡{"done":true}︡
︠17ad0071-9851-4b04-a6d9-ef0c0d935841i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.

︡0c9acd72-90a4-45b7-ae8b-73eb90051e8b︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠1a6e6fe5-047e-402a-8554-663a60756d62s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡650576a1-1e29-460f-94bb-c5e00fb3629f︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{3 \\, {\\left(4 \\, \\xi + 3\\right)} e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} - \\frac{{\\left(8 \\, \\xi - 7\\right)} e^{\\left(3 \\, \\xi\\right)} + 16 \\, \\xi + 16}{16 \\, \\xi^{3}} \\geq 0.521$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)} + 8 \\, \\xi}{16 \\, \\xi^{3}} + \\frac{3 \\, e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} \\geq \\left(-0.0120\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(8 \\, \\xi - 5\\right)} e^{\\left(3 \\, \\xi\\right)} + 4 \\, \\xi + 8}{16 \\, \\xi^{3}} - \\frac{3 \\, e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} \\geq 0.248$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{3 \\, {\\left(4 \\, \\xi + 3\\right)} e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} + \\frac{{\\left(4 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} - 4 \\, \\xi - 8}{16 \\, \\xi^{3}} \\geq 0.764$</div>"}︡{"stdout":"Surface is not stable!\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)} + 8 \\, \\xi}{16 \\, \\xi^{3}} + \\frac{3 \\, e^{\\left(-\\xi\\right)}}{16 \\, \\xi^{3}} \\leq \\left(-0.00537\\right)$</div>"}︡{"done":true}︡
︠ea67bacd-6f7d-4b4b-b670-ed79251fbd43i︠
%md 
------------------
#### No. 14: Degree $3$ / Singularity type $E_6$. <a id="no14"></a>
The combinatorial data is is given by $\Box=[-1,5]$ and $\Phi_0(u) = \min \{\frac{u-1}{3},0\},\; \Phi_\infty(u)= \frac{u-2}{3},\; \Phi_1(u) = \frac{-u-1}{2}$
︡234f9391-4f6c-43fe-8830-d2114a7eabdf︡{"done":true,"md":"------------------\n#### No. 14: Degree $3$ / Singularity type $E_6$. <a id=\"no14\"></a>\nThe combinatorial data is is given by $\\Box=[-1,5]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{3},0\\},\\; \\Phi_\\infty(u)= \\frac{u-2}{3},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠528f666d-5fd9-4766-8410-b26e61678ec6s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-2)/3,0)
Phi2(u)=(u-2)/3
Phi3(u)=(-u-1)/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡fc4a8b25-ca84-4917-9020-0dfb49333cfc︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{3} \\, u - \\frac{2}{3}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠82c53c32-60c6-42c2-9922-302826250570i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡18b2faec-7374-44f9-9dd1-d1ff2f81e9d6︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠ecd0cc59-2a51-4e9d-bc78-6a3efc21cee7s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡12d515bc-5325-4f66-a5a7-1b5043e0fca0︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi^{2} + \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{6 \\, \\xi^{3}} - \\frac{{\\left(4 \\, {\\left(\\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠61004fbc-70c6-4394-98ea-7759928a6642i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡968790ef-a629-41e3-a3e2-4f250e693125︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠17e49674-f9d0-4f17-bbf3-1625b930d697s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡6c112eb3-769f-41c2-87ef-cc6c89e971de︡{"stdout":"Numerical solution: -1.95932042376\n"}︡{"stdout":"Interval containing solution: [-1.95932055 .. -1.95932030]\n"}︡{"done":true}︡
︠62c89a4b-2343-4375-839e-e1d0b9f3d481i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡1ef0f5d3-107d-4969-83fd-e87fbbc11f12︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠77dae45b-02e8-4943-8101-436b9674040bs︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡4609a523-4004-41dc-bdda-bcc13951b549︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(10 \\, \\xi^{2} + 4 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(4 \\, {\\left(3 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + 3\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.07627603$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(14 \\, \\xi^{2} + 2 \\, \\xi - 5\\right)} e^{\\left(3 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(3 \\, \\xi - 4\\right)} e^{\\left(3 \\, \\xi\\right)} + 3\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.07668713$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(14 \\, \\xi^{2} + 2 \\, \\xi - 5\\right)} e^{\\left(3 \\, \\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{{\\left(12 \\, {\\left(\\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + 6 \\, \\xi + 7\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1276748$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(10 \\, \\xi^{2} + 4 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(6 \\, \\xi e^{\\left(3 \\, \\xi\\right)} - 6 \\, \\xi - 1\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.2806380$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠6f2abbf8-f3dc-4f40-88dc-c38aad274c86i︠
%md 
------------------
#### No. 15: Degree $3$ / Singularity type $A_4A_1$. <a id="no15"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{\frac{u-1}{2},0\},\; \Phi_\infty(u)= \min \{-u,0\},\; \Phi_1(u) = \frac{u-2}{3}$
︡9593d06a-bd6e-452d-9acf-d8bc067eae2b︡{"done":true,"md":"------------------\n#### No. 15: Degree $3$ / Singularity type $A_4A_1$. <a id=\"no15\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_\\infty(u)= \\min \\{-u,0\\},\\; \\Phi_1(u) = \\frac{u-2}{3}$"}
︠2ad4b1d8-d998-4677-a53b-bacf3445bed0s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(1/2*u-1/2,0)
Phi2(u)=min_symbolic(0,-u)
Phi3(u)=1/3*u-2/3

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡edf01262-cfb6-47d0-8c1e-57612173e555︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, -u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"done":true}︡
︠5e7031f1-6bdf-4e8f-8e47-ec6365d3e9a7i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡b0e4594e-55c9-44a3-a4fb-ea6b1726959a︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠a5da7651-24bb-4325-96b3-ae2402c45db2s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡9128e398-ad16-48ff-99e4-00c83bbc5cc1︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(3 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 5 \\, \\xi + 10\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}} + \\frac{2 \\, {\\left(2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 3\\right)}}{3 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠17a300f5-5433-4b6f-a6b2-5850de7aaf82i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡0b97c066-209d-432c-9c14-54bbfbe5c9ba︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠0dfc7d55-29e7-45b2-b7e3-6cb855a7a96bs︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡9c8b0e36-593b-4b3b-a6f4-be0ac53b776c︡{"stdout":"Numerical solution: -1.19618719651\n"}︡{"stdout":"Interval containing solution: [-1.19618732 .. -1.19618707]\n"}︡{"done":true}︡
︠21a6e357-4f86-4301-aef1-aca7eab0e362i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡229160cc-cd89-443c-8d8e-7ff8748b96c2︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠02e2c9b9-0ad9-4888-b111-e5a1958e44bcs︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡4631bf8d-9039-4cfd-bf69-1f22e2b5ff41︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(9 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 5\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 3 \\, \\xi + 3}{9 \\, \\xi^{3}} \\geq 0.2231133$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, {\\left(4 \\, \\xi - 7\\right)} e^{\\left(2 \\, \\xi\\right)} + 30 \\, \\xi + 25\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{2 \\, {\\left(3 \\, \\xi - 4\\right)} e^{\\left(2 \\, \\xi\\right)} + 9 \\, \\xi + 9}{9 \\, \\xi^{3}} \\geq 0.5531086$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(27 \\, \\xi e^{\\left(2 \\, \\xi\\right)} - 18 \\, {\\left(\\xi + 2\\right)} e^{\\xi} + 10\\right)} e^{\\left(-\\xi\\right)}}{72 \\, \\xi^{3}} + \\frac{16 \\, {\\left(3 \\, \\xi - 4\\right)} e^{\\left(2 \\, \\xi\\right)} - 27 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 54 \\, \\xi + 36}{72 \\, \\xi^{3}} \\geq 0.03862803$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, {\\left(5 \\, \\xi - 8\\right)} e^{\\left(2 \\, \\xi\\right)} - 18 \\, {\\left(\\xi + 2\\right)} e^{\\xi} + 60 \\, \\xi + 50\\right)} e^{\\left(-\\xi\\right)}}{72 \\, \\xi^{3}} + \\frac{16 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} - 27 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi - 12}{72 \\, \\xi^{3}} \\geq 0.8148505$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠dd407ee9-6fb6-4ee2-8b56-d27629364871i︠
%md 
------------------
#### No. 17: Degree $3$ / Singularity type $D_5$. <a id="no17"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{\frac{u-2}{3},0\},\; \Phi_\infty(u)= \min \{\frac{u-1}{2},0\},\; \Phi_1(u) = \frac{-u-1}{2}$
︡f68466a7-6262-4649-af96-d29565e5533c︡{"done":true,"md":"------------------\n#### No. 17: Degree $3$ / Singularity type $D_5$. <a id=\"no17\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{\\frac{u-2}{3},0\\},\\; \\Phi_\\infty(u)= \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠f65179c5-92cb-4237-930e-f4fbe799f13as︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-2)/3,0)
Phi2(u)=min_symbolic((u-1)/2,0)
Phi3(u)=(-u-1)/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡dc387937-fe89-4d42-a2cd-84132fac836d︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{3} \\, u - \\frac{2}{3}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠32f10d7d-f25d-4194-891f-56fd8a4e5a38i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡76a8695e-5d75-4b26-9750-6443ac39fbcb︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠2005e8d0-2e26-4c52-8751-3c5d5ce05e41s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡e7237c0e-6365-4c85-9a9c-1b41b55ba3ef︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{2 \\, \\xi^{3}} - \\frac{{\\left(4 \\, {\\left(\\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + 3 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, \\xi + 4\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠09e2e679-7934-41a6-a667-2425600756e6i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡a063ebe9-f258-4fb7-9cdd-78e6e99953bb︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠2258b294-50be-4c98-952c-3b45b22399f6s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡8f159749-fac0-4656-8b55-536250895b16︡{"stdout":"Numerical solution: -1.83879135601\n"}︡{"stdout":"Interval containing solution: [-1.83879146 .. -1.83879125]\n"}︡{"done":true}︡
︠f18ef512-2bb2-4def-8c8b-61dbbe62d9c7i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡fcfb7c4e-d20a-45bd-bb59-1c51c7a6adec︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠510a9c40-dabd-40ac-b04c-a081f8dc73abs︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡d126893a-8ec5-4ea3-8b69-ccad2b4cd949︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(4 \\, {\\left(3 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} - 9 \\, e^{\\left(2 \\, \\xi\\right)} + 4\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1038101$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(3 \\, \\xi - 4\\right)} e^{\\left(3 \\, \\xi\\right)} + 9 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 8\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1934829$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(27 \\, \\xi - 14\\right)} e^{\\left(3 \\, \\xi\\right)} + 30 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 24 \\, \\xi + 32\\right)} e^{\\left(-\\xi\\right)}}{72 \\, \\xi^{3}} - \\frac{6 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, {\\left(\\xi + 2\\right)} e^{\\xi}}{24 \\, \\xi^{3}} \\geq 0.2032122$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(9 \\, \\xi - 10\\right)} e^{\\left(3 \\, \\xi\\right)} + 6 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 24 \\, \\xi - 8\\right)} e^{\\left(-\\xi\\right)}}{72 \\, \\xi^{3}} + \\frac{6 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} - {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} - 2 \\, {\\left(\\xi + 2\\right)} e^{\\xi}}{24 \\, \\xi^{3}} \\geq 0.5005054$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠5f4164a2-7e73-44dc-a47d-73790260eef0i︠
%md 
------------------
#### No. 19: Degree $3$ / Singularity type $D_4$. <a id="no19"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{-u,\frac{-u-1}{2}\},\; \Phi_\infty(u)= \min \{0,\frac{u-1}{2}\},\; \Phi_1(u) = \min \{0,\frac{u-1}{2}\}$
︡808f583c-8226-4665-be72-e13535d03cd8︡{"done":true,"md":"------------------\n#### No. 19: Degree $3$ / Singularity type $D_4$. <a id=\"no19\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{-u,\\frac{-u-1}{2}\\},\\; \\Phi_\\infty(u)= \\min \\{0,\\frac{u-1}{2}\\},\\; \\Phi_1(u) = \\min \\{0,\\frac{u-1}{2}\\}$"}
︠e0d274ae-ea64-4ee0-bcc8-4120390cc1fds︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((-u-1)/2,-u)
Phi2(u)=min_symbolic((u-1)/2,0)
Phi3(u)=min_symbolic((u-1)/2,0)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡6badc479-5619-4d75-a0ce-d15cab6c33d9︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-\\frac{1}{2} \\, u - \\frac{1}{2}, -u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"done":true}︡
︠9bbce312-c0e1-45b1-b5e8-ce9be101de06i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡1d157828-30f3-4265-9bb6-2f25d9bf79f4︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠7202d7e1-c044-475c-a60a-6fb719c45053s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡2cd58f19-3fb7-4d55-8865-bdb8720c8482︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(2 \\, \\xi^{2} - \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}} + \\frac{2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + {\\left(\\xi^{2} - 2 \\, \\xi + 2\\right)} e^{\\xi}}{\\xi^{3}}$</div>"}︡{"done":true}︡
︠ae254d03-f5d2-4549-a3a2-764466ee3647i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡6335202b-7f74-4a78-ab96-84715ad985d1︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠a0f28581-ba65-4f0d-bd3a-8e271306b851s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡0affb7c0-fbc8-46fa-b01e-895ccd4b862d︡{"stdout":"Numerical solution: -1.69131741884\n"}︡{"stdout":"Interval containing solution: [-1.69131753 .. -1.69131729]\n"}︡{"done":true}︡
︠b006292b-405d-43ff-8aef-016d788dca9ei︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡6218e3ad-0591-433a-8f97-b49dab35be42︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠694297b5-45d4-4adf-8f8b-752ebb07b80fs︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡8064def8-e207-44c5-b5d3-4ca3780f2d6e︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(2 \\, \\xi^{2} - 4 \\, \\xi + 3\\right)} e^{\\left(2 \\, \\xi\\right)} - 2 \\, \\xi - 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - {\\left(\\xi^{2} - 2\\right)} e^{\\xi}}{2 \\, \\xi^{3}} \\geq 0.2194796$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi^{2} - 2 \\, \\xi + 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 1\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - {\\left(\\xi^{2} - 2\\right)} e^{\\xi}}{2 \\, \\xi^{3}} \\geq 0.2194797$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi^{2} - 2 \\, \\xi + 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 1\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - {\\left(\\xi^{2} - 2\\right)} e^{\\xi}}{2 \\, \\xi^{3}} \\geq 0.2194797$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi^{2} - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, \\xi + 1\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - {\\left(\\xi^{2} - 2\\right)} e^{\\xi}}{2 \\, \\xi^{3}} \\geq 0.6584392$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠64407fd3-9fd0-4e46-a686-b6a73793a331i︠
%md 
------------------
#### No. 20: Degree $3$ / Singularity type $2A_1A_3$. <a id="no20"></a>
The combinatorial data is is given by $\Box=[-1,1]$ and $\Phi_0(u) = \min \{0,u\},\; \Phi_\infty(u)= \frac{u-1}{2},\; \Phi_1(u) = \frac{-u-1}{2}$
︡05f07b10-2595-4161-bd0e-3cde2c19c9cd︡{"done":true,"md":"------------------\n#### No. 20: Degree $3$ / Singularity type $2A_1A_3$. <a id=\"no20\"></a>\nThe combinatorial data is is given by $\\Box=[-1,1]$ and $\\Phi_0(u) = \\min \\{0,u\\},\\; \\Phi_\\infty(u)= \\frac{u-1}{2},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠f3256b96-6f5a-453b-aab9-26aea941ac47s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,1]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,u)
Phi2(u)=(u-1)/2
Phi3(u)=(-u-1)/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡265a1eba-653d-400a-ad94-d97bd813eb7e︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 1$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠ea954cd6-21de-49bc-baec-22999f002712i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡d0d3c23a-e347-4477-b27f-93fe422134ff︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠41ce9d2c-dd85-4086-9838-8f9b02296e41s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡c22f5b35-6dcb-4301-9398-272c36546abe︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(\\xi - 1\\right)} e^{\\xi}}{\\xi^{2}} - \\frac{{\\left(\\xi - 2 \\, e^{\\xi} + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}}$</div>"}︡{"done":true}︡
︠bc164403-9207-4ae2-9538-3ba8c0f89158i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡4c12f83c-e011-47d1-b79b-5e48b088d051︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠0293eb55-c855-43cc-9433-9e1b0e5c39eds︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡db04a2df-f29f-4f2c-a1d1-926d4c32166f︡{"stdout":"Numerical solution: -0.944683075826\n"}︡{"stdout":"Interval containing solution: [-0.944683180 .. -0.944682970]\n"}︡{"done":true}︡
︠af70cdd8-ad9a-4e92-aa5e-fe146b451432i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡063b4347-0896-4dac-a80d-a6cd8e8da0c0︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠f81bfa0b-9442-4513-a6a7-693970fd4f3ds︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡b5014a2a-5d48-4e7a-8669-7de4f16b6318︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{e^{\\xi}}{2 \\, \\xi} - \\frac{{\\left({\\left(\\xi - 1\\right)} e^{\\xi} + 1\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} \\geq 0.5383074$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(\\xi - 1\\right)} e^{\\xi}}{2 \\, \\xi^{2}} + \\frac{1}{2 \\, \\xi^{2}} \\geq 0.1366506$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(\\xi - 1\\right)} e^{\\xi}}{2 \\, \\xi^{2}} + \\frac{{\\left({\\left(\\xi - 4\\right)} e^{\\xi} + 2 \\, \\xi + 4\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}} \\geq 0.1366499$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{e^{\\xi}}{2 \\, \\xi} + \\frac{{\\left(\\xi - e^{\\xi} + 1\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} \\geq 0.8116092$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠ab84eb09-c3f8-4147-96cd-0646d43bf137i︠
%md 
------------------
#### No. 21: Degree $4$ / Singularity type $D_5$. <a id="no21"></a>
The combinatorial data is is given by $\Box=[-1,5]$ and $\Phi_0(u) = \min \{\frac{u-1}{2},0\},\; \Phi_\infty(u)= \frac{u-2}{3},\; \Phi_1(u) = \frac{-u-1}{2}$
︡925182c8-f1d3-4878-8e01-35567d3a75fb︡{"done":true,"md":"------------------\n#### No. 21: Degree $4$ / Singularity type $D_5$. <a id=\"no21\"></a>\nThe combinatorial data is is given by $\\Box=[-1,5]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_\\infty(u)= \\frac{u-2}{3},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠f013cfca-d33e-4b53-8294-ea741c9e6022s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,5]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-1)/2,0)
Phi2(u)=(u-2)/3
Phi3(u)=(-u-1)/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡0271854a-eb07-4873-af73-aae3ffbdd258︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 5$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠7d9c754d-213e-4614-8c71-48aa8e24f50ai︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡e49fa8f0-f8e3-41df-b0af-7fb0bfba858d︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠1a52ff1f-53ca-4068-b8eb-724c030928f8s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡7bd52a4e-03db-4fc1-980e-768c07248e0d︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(5 \\, \\xi - 2\\right)} e^{\\left(5 \\, \\xi\\right)}}{6 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, \\xi + 4\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠c07ce586-0f7c-455e-886a-3105d20a5124i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡5ad7cbaf-0a6d-4716-b30e-d440b7525b1c︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠6d6558dc-adb9-4b35-add3-19aaca1f9a76s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡4111c9d0-bbc0-4f6c-b9a8-c79f24258008︡{"stdout":"Numerical solution: -1.85969017295\n"}︡{"stdout":"Interval containing solution: [-1.85969028 .. -1.85969007]\n"}︡{"done":true}︡
︠cb1f4b9a-45b5-42ea-bb73-e67e39e0ffb1i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡594389c6-7d13-426b-8987-c95a679e3d81︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠73283577-364b-4816-8bd4-7bc5391bfce5s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡29d13c03-5107-4c61-aecb-5c0dcc42ef35︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(9 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 8\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1933190$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(12 \\, \\xi - 5\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{{\\left(9 \\, e^{\\left(2 \\, \\xi\\right)} - 4\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1048980$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(12 \\, \\xi - 5\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{{\\left(3 \\, {\\left(4 \\, \\xi - 7\\right)} e^{\\left(2 \\, \\xi\\right)} + 12 \\, \\xi + 16\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.2043036$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(6 \\, \\xi - 1\\right)} e^{\\left(5 \\, \\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(3 \\, {\\left(2 \\, \\xi + 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 12 \\, \\xi - 4\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.5025207$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠c940197e-4f84-489c-8e5e-52c4d1a377e1i︠
%md 
------------------
#### No. 22: Degree $4$ / Singularity type $A_3A_1$. <a id="no22"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{0,u\},\; \Phi_\infty(u)= \min \{u,u\},\; \Phi_1(u) = \frac{-2u-2}{3}$
︡bf23f35a-59bd-4459-a830-373746a470b6︡{"done":true,"md":"------------------\n#### No. 22: Degree $4$ / Singularity type $A_3A_1$. <a id=\"no22\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{0,u\\},\\; \\Phi_\\infty(u)= \\min \\{u,u\\},\\; \\Phi_1(u) = \\frac{-2u-2}{3}$"}
︠3718005f-a136-4f72-b929-6295523ab9f1s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,u)
Phi2(u)=min_symbolic(0,u)
Phi3(u)=-2/3*u-2/3

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡51aab9b4-f2b3-4b09-bdfc-211f51558aa4︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{2}{3} \\, u - \\frac{2}{3}$</div>"}︡{"done":true}︡
︠e38a9020-5a92-42a3-aef6-5ccdf57ab861i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡46f0b33b-94bb-4b43-82dc-94063c48a336︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠0944eae6-9d35-42bb-bdaa-ae7493db5a29s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡f54efbbd-85fe-4d54-8d84-bba7c0ffcb2d︡{"html":"<div align='center'>$\\displaystyle \\frac{4 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)}}{3 \\, \\xi^{3}} - \\frac{4 \\, {\\left(\\xi - 3 \\, e^{\\xi} + 2\\right)} e^{\\left(-\\xi\\right)}}{3 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠641178a2-a48d-4586-82b5-3b8aa60b997bi︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡835abc4e-2548-43ae-b4cd-69ade69c2cd7︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠95f25990-24b8-4c74-8d14-263fa7db676es︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡1f9b476b-2ac0-4c6a-97e1-48db96a981bf︡{"stdout":"Numerical solution: -0.97052220918\n"}︡{"stdout":"Interval containing solution: [-0.970522315 .. -0.970522105]\n"}︡{"done":true}︡
︠b659f2e0-f50c-4d7e-ab1a-c168a16a23bai︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡4527a533-b3b1-42c7-8f07-adc6549605b7︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠adf0dd41-e367-4953-9ddf-128325b4b3b5s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡d999613a-f7f9-4846-b8c2-876000c51df1︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{2 \\, {\\left(3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 4\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq 0.5714598$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} - \\frac{2 \\, {\\left(3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 4\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq 0.5714598$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} + \\frac{2 \\, {\\left(9 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 16\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq \\left(-0.2018673\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} + \\frac{2 \\, {\\left(3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 8\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\geq 0.9410544$</div>"}︡{"stdout":"Surface is not stable!\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{2 \\, {\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)}}{9 \\, \\xi^{3}} + \\frac{2 \\, {\\left(9 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 16\\right)} e^{\\left(-\\xi\\right)}}{9 \\, \\xi^{3}} \\leq \\left(-0.2018636\\right)$</div>"}︡{"done":true}︡
︠b1fa23df-d877-483f-9a8a-49bf64b5a1f7i︠
%md 
------------------
#### No. 23: Degree $4$ / Singularity type $D_4$. <a id="no23"></a>
The combinatorial data is is given by  $\Box=[-1,3]$ and $\Phi_0(u) = \min \{\frac{u-1}{2},0\},\; \Phi_\infty(u)= \min \{\frac{u-1}{2},0\},\; \Phi_1(u) = \frac{-u-1}{2}$
︡7287a7c7-9f67-4956-a0a8-16d101391f7c︡{"done":true,"md":"------------------\n#### No. 23: Degree $4$ / Singularity type $D_4$. <a id=\"no23\"></a>\nThe combinatorial data is is given by  $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_\\infty(u)= \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠c4f6c6f1-a5eb-45ef-a94f-199c7896e3d7s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic((u-1)/2,0)
Phi2(u)=min_symbolic((u-1)/2,0)
Phi3(u)=(-u-1)/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡72037d46-8633-460e-ba31-e8c83fe6c10b︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠57735631-eef3-465d-a19d-e1308fa2a2c6i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡488ea316-1e6d-47bf-90f7-8159053527aa︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠ee7daf50-e5f0-4d10-8dbf-44c1dfb59a53s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡f73f1a7a-18d4-484c-8025-aaa77a8ecfee︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{2 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠ceae5ee1-6f8e-4ad1-9207-e83ead0a2df5i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡2e8a1c6d-a725-4513-aa05-18e12f6e1d44︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠34caf0a7-9f5d-4cb5-b1fa-2b36d8f1b99as︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡e63d36c6-8577-430f-9b80-85f33b29e43d︡{"stdout":"Numerical solution: -1.79675598472\n"}︡{"stdout":"Interval containing solution: [-1.79675609 .. -1.79675588]\n"}︡{"done":true}︡
︠96387750-ff1d-4ccc-b3a8-4b3472234c8fi︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡69335c65-731b-476c-a729-4434f89d0e31︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠64411282-33df-44a0-bee9-71ab46d83db4s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡dd799130-a678-4aca-b483-a51d0790c952︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 1\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.2208160$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 1\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.2208160$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(4 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, \\xi + 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.2333050$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi + 1\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.6749371$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠d4d7c642-c09a-4417-a33d-6b809ec42037i︠
%md 
------------------
#### No. 24: Degree $4$ / Singularity type $A_4$. <a id="no24"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{\frac{u-2}{3},0\},\; \Phi_\infty(u)= \min \{-u,0\},\; \Phi_1(u) = \frac{-u-1}{2}$
︡3fd81938-7ec0-443a-a23e-1eb8359ef14d︡{"done":true,"md":"------------------\n#### No. 24: Degree $4$ / Singularity type $A_4$. <a id=\"no24\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{\\frac{u-2}{3},0\\},\\; \\Phi_\\infty(u)= \\min \\{-u,0\\},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠7de2bab4-68b4-4b02-b6c1-f8238c8caabbs︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,1/3*u-2/3)
Phi2(u)=min_symbolic(-u,0)
Phi3(u)=1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡3615b56c-13f4-4101-af62-5585272b9f43︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, \\frac{1}{3} \\, u - \\frac{2}{3}\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-u, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠50bffa8b-4ebd-4619-8866-3c1697e0fbeci︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡b5765f3c-d805-48c2-9bc1-1b9e257d5544︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠2dc84852-9259-417a-85a9-9fc66ccdae42s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡c2628bc0-8c2c-4aaf-b1df-cc1eb9534921︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(4 \\, {\\left(\\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + 5 \\, \\xi + 10\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}} + \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} + 4}{2 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠52d1620a-7852-4712-aee4-0c9f7f77efb2i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡b5f9d628-3d49-45ba-8329-2f97e21bb7ee︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠92f80bee-4b64-45e5-b3f2-764d9cae5541s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡cfd43170-e2e6-4a9a-b384-3af4fb70473d︡{"stdout":"Numerical solution: -1.38176017478\n"}︡{"stdout":"Interval containing solution: [-1.38176030 .. -1.38176006]\n"}︡{"done":true}︡
︠4e4873f4-7e5d-4686-9fab-07777c3a5eedi︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡018e2830-27d4-48ec-b489-67246faeb8a0︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠3b0c8a36-3dba-4ce2-874b-b969c260f8bbs︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡eeaadf3e-0dcf-495d-85b1-fe7f64fa14db︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(4 \\, {\\left(3 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} - 5\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + 2 \\, \\xi}{4 \\, \\xi^{3}} \\geq 0.04422930$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, {\\left(9 \\, \\xi - 8\\right)} e^{\\left(3 \\, \\xi\\right)} + 30 \\, \\xi + 25\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)} + 4 \\, \\xi + 4}{4 \\, \\xi^{3}} \\geq 0.5594457$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, {\\left(3 \\, \\xi + 1\\right)} e^{\\left(3 \\, \\xi\\right)} - 12 \\, {\\left(\\xi + 1\\right)} e^{\\xi} - 5\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{3 \\, {\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)} - 6 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 8 \\, \\xi + 8}{12 \\, \\xi^{3}} \\geq 0.2463752$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, {\\left(6 \\, \\xi - 5\\right)} e^{\\left(3 \\, \\xi\\right)} - 12 \\, {\\left(\\xi + 1\\right)} e^{\\xi} + 30 \\, \\xi + 25\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{3 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} - 6 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, \\xi - 4}{12 \\, \\xi^{3}} \\geq 0.8500507$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠30fa1007-9dde-4cb8-a149-4aa7f9e695f7i︠
%md 
------------------
#### No. 26: Degree $4$ / Singularity type $A_3$. <a id="no26"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{-u,0\},\; \Phi_\infty(u)= \min \{0,\frac{u-1}{2}\},\; \Phi_1(u) = \min \{0,\frac{u-1}{2}\}$
︡22188dcb-0d11-4c2d-8137-c3d8db3c1088︡{"done":true,"md":"------------------\n#### No. 26: Degree $4$ / Singularity type $A_3$. <a id=\"no26\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{-u,0\\},\\; \\Phi_\\infty(u)= \\min \\{0,\\frac{u-1}{2}\\},\\; \\Phi_1(u) = \\min \\{0,\\frac{u-1}{2}\\}$"}
︠967bfd60-9532-4cd6-87da-51d6a5cbfbces︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(-u,0)
Phi2(u)=min_symbolic((u-1)/2,0)
Phi3(u)=min_symbolic((u-1)/2,0)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡b3ae9888-50ac-405c-a3e3-70b650fff63d︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-u, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(\\frac{1}{2} \\, u - \\frac{1}{2}, 0\\right)$</div>"}︡{"done":true}︡
︠e20efbc7-80be-47ab-8f3d-d8ea6e7ef9f7i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡4857328e-145f-4eb3-85f1-201020ca97d5︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠05037f6a-337b-43e2-9296-8974df91a7f5s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡42cbde4e-04a5-4480-a9b6-8c5ac7d1dab4︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} + \\frac{2 \\, {\\left({\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 1\\right)}}{\\xi^{3}}$</div>"}︡{"done":true}︡
︠7147c592-2a68-4dc0-b7b3-7950bf639d74i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡9bfeefef-3e3c-4010-9e77-dfc3c22529cf︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠03759619-a990-4703-9acd-ffca9d46d1f1s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡7245d48c-467e-4975-85b1-2b3acd74e608︡{"stdout":"Numerical solution: -1.31047894021\n"}︡{"stdout":"Interval containing solution: [-1.31047905 .. -1.31047883]\n"}︡{"done":true}︡
︠afd4ad99-ab73-402e-8a40-b86165a49ce3i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡9bbd50ce-09c6-43ba-aa9c-35e494adeffb︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠a6435a7f-0dac-4787-ad3f-644ad072142cs︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡b16c25a5-310f-4077-8ca0-c5fed1debb73︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} - \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1}{\\xi^{3}} \\geq 0.5757800$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{8 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 4}{8 \\, \\xi^{3}} - \\frac{{\\left(\\xi - 2\\right)} e^{\\xi} + 2 \\, \\xi + 4}{8 \\, \\xi^{3}} \\geq 0.1674627$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{8 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 4}{8 \\, \\xi^{3}} - \\frac{{\\left(\\xi - 2\\right)} e^{\\xi} + 2 \\, \\xi + 4}{8 \\, \\xi^{3}} \\geq 0.1674627$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(3 \\, \\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} - 2 \\, {\\left(\\xi + 2\\right)} e^{\\xi} + 4 \\, \\xi + 4\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{4 \\, {\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 2 \\, \\xi}{4 \\, \\xi^{3}} \\geq 0.9107058$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠ddedb0f3-038c-4004-97e8-2a14d689fd4bi︠
%md 
------------------
#### No. 28: Degree $4$ / Singularity type $A_1A_2$. <a id="no28"></a>
The combinatorial data is is given by  $\Box=[-1,1]$ and $\Phi_0(u) = \min \{0,u\},\; \Phi_\infty(u)= \min \{0,u\},\; \Phi_1(u) = \frac{-u-1}{2}$
︡1167be2c-5c3f-4e9d-8380-0c3ed3a2ab4a︡{"done":true,"md":"------------------\n#### No. 28: Degree $4$ / Singularity type $A_1A_2$. <a id=\"no28\"></a>\nThe combinatorial data is is given by  $\\Box=[-1,1]$ and $\\Phi_0(u) = \\min \\{0,u\\},\\; \\Phi_\\infty(u)= \\min \\{0,u\\},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠2055f703-ef9b-4d09-a31c-0e13d070917fs︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,1]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,u)
Phi2(u)=min_symbolic(0,u)
Phi3(u)=-1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡8684dce7-9866-4cad-a250-7bc4ca6634ab︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 1$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠b3801d3b-6317-4904-bd63-4a77a4d6d530i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡d3950317-65af-43d1-8aa6-205d75de31cc︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠dd041236-becf-45dc-a4a4-f60adff9bc14s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡b1990c28-e3b9-4f21-8619-67a238f63f17︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(3 \\, \\xi - 8 \\, e^{\\xi} + 6\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi^{2} - \\xi - 2\\right)} e^{\\xi}}{2 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠6110cd14-0706-4199-a68f-df8a6a5e6573i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡bab10c3e-eaaf-4729-b5a0-f9e757a19562︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠924b1b6e-3bea-4428-89d3-8685199626e1s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡43d4eb50-de11-4d6e-92b8-efcd551d4aaf︡{"stdout":"Numerical solution: -0.743738705959\n"}︡{"stdout":"Interval containing solution: [-0.743738816 .. -0.743738591]\n"}︡{"done":true}︡
︠e7b113bf-4228-4322-ba5d-93cf3907db2ai︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡3891f1a8-1b65-4f1a-8aa1-f8c4051ccaef︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠53c235d7-eedf-4b39-a705-79123c6e1c6fs︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡4575e176-647f-4fb6-840d-d4a8e502328f︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi^{2} - 1\\right)} e^{\\xi}}{4 \\, \\xi^{3}} \\geq 0.4699562$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi^{2} - 1\\right)} e^{\\xi}}{4 \\, \\xi^{3}} \\geq 0.4699562$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(8 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 15\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, \\xi^{2} - 1\\right)} e^{\\xi}}{4 \\, \\xi^{3}} \\geq \\left(-0.1023106\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(4 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 9\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi^{2} - 1\\right)} e^{\\xi}}{4 \\, \\xi^{3}} \\geq 0.8376046$</div>"}︡{"stdout":"Surface is not stable!\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(8 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 15\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, \\xi^{2} - 1\\right)} e^{\\xi}}{4 \\, \\xi^{3}} \\leq \\left(-0.1023037\\right)$</div>"}︡{"done":true}︡
︠b12a50ed-7b7e-4604-91de-70d036c9c207i︠
%md 
------------------
#### No. 29: Degree $5$ / Singularity type $A_4$. <a id="no29"></a>
The combinatorial data is is given by $\Box=[-1,5]$ and $\Phi_0(u) = \min \{-u,0\},\; \Phi_\infty(u)= \frac{u-2}{3},\; \Phi_1(u) = \frac{u-1}{2}$
︡1ed5d5ef-bd77-40c5-8224-ecdad76a3a18︡{"done":true,"md":"------------------\n#### No. 29: Degree $5$ / Singularity type $A_4$. <a id=\"no29\"></a>\nThe combinatorial data is is given by $\\Box=[-1,5]$ and $\\Phi_0(u) = \\min \\{-u,0\\},\\; \\Phi_\\infty(u)= \\frac{u-2}{3},\\; \\Phi_1(u) = \\frac{u-1}{2}$"}
︠e9e6aed6-4cbe-4491-a472-35e88c2d67a4s︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,1]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,-u)
Phi2(u)=1/3*u-2/3
Phi3(u)=1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡71bf1410-1bd7-41c7-83e5-41f856d6124b︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 1$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, -u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{3} \\, u - \\frac{2}{3}$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠91e20549-61fc-480f-bb71-9ac4e038db87is︠
%hide
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡559be631-b75d-4fa6-9379-eb2b0e43ae15︡{"hide":"input"}︡{"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}︡{"done":true}︡
︠c4b4fcb6-64f5-4cdb-a04a-4ca44fc605d6s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡12c2e63b-ccc7-444b-84f8-791493e50433︡{"html":"<div align='center'>$\\displaystyle -\\frac{5 \\, {\\left(\\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{6 \\, \\xi^{3}} + \\frac{{\\left(4 \\, \\xi^{2} - 3 \\, \\xi - 2\\right)} e^{\\xi} + 12}{6 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠113419e7-72a7-483f-bfeb-500e1e5b0de4i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡a9c154a2-006d-4ba9-a51d-d8ac94e96aba︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠a5b81ebe-f779-4213-907f-fbcc2865d370s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡c01650e1-4bab-4c1a-9b3f-e44ea8f29b9a︡{"stdout":"Numerical solution: -0.830799974476\n"}︡{"stdout":"Interval containing solution: [-0.830800087 .. -0.830799862]\n"}︡{"done":true}︡
︠191ff066-f3ea-4652-be25-214140263fb1i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡b662d9cb-4633-4dd7-bdf8-e4031299fe02︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠9bd9752f-8094-4735-b351-af878791e953s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡a4655a1b-ec5f-43d9-a672-9f0111f17ffd︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{5 \\, {\\left(6 \\, \\xi + 5\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} - \\frac{{\\left(8 \\, \\xi^{2} - 20 \\, \\xi - 11\\right)} e^{\\xi} + 36 \\, \\xi + 36}{36 \\, \\xi^{3}} \\geq 0.5216795$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(8 \\, \\xi^{2} - 8 \\, \\xi - 5\\right)} e^{\\xi} + 18 \\, \\xi}{36 \\, \\xi^{3}} + \\frac{5 \\, e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.01721630$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(16 \\, \\xi^{2} - 10 \\, \\xi - 7\\right)} e^{\\xi} + 12 \\, \\xi + 12}{36 \\, \\xi^{3}} - \\frac{5 \\, e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} \\geq 0.1968489$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{5 \\, {\\left(6 \\, \\xi + 5\\right)} e^{\\left(-\\xi\\right)}}{36 \\, \\xi^{3}} + \\frac{{\\left(16 \\, \\xi^{2} + 2 \\, \\xi - 1\\right)} e^{\\xi} - 6 \\, \\xi - 24}{36 \\, \\xi^{3}} \\geq 0.7357463$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠3d8c44f7-66f5-45f2-8943-2550a2559923i︠
%md 
------------------
#### No. 30: Degree $5$ / Singularity type $A_3$. <a id="no30"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{\frac{u-1}{2},0\},\; \Phi_\infty(u)= \min \{0,u\},\; \Phi_1(u) = \frac{-u-1}{2}$
︡aa95206b-3106-423a-84dd-e2d49d9af8e8︡{"done":true,"md":"------------------\n#### No. 30: Degree $5$ / Singularity type $A_3$. <a id=\"no30\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{\\frac{u-1}{2},0\\},\\; \\Phi_\\infty(u)= \\min \\{0,u\\},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠1a213cf6-f9ce-45a8-b89d-abafcad3130as︠
# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,1/2*u-1/2)
Phi2(u)=min_symbolic(-u,0)
Phi3(u)=1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡91b256a1-d970-427b-91a8-fb1bd5638554︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, \\frac{1}{2} \\, u - \\frac{1}{2}\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-u, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠1ca38c07-1062-4236-90f0-a2405037dd60i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡3040a26c-8066-4971-a8a8-aaf5565090a0︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠9351f8b6-81d0-428b-a836-9ffc62a2abe8s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡a71a2683-57ca-42e1-9c80-82128ca89523︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, \\xi + 4\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}} + \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)} + 4}{2 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠a2eeee5d-d5a2-429b-af9f-02692d0d464fi︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡5ae8c462-29d3-42e2-a51d-aad7e23e2e0e︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠f9eaab96-ea2a-4fc4-9c74-abaf9d1f66f8s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡6f2cf0bc-3009-4fd6-96ea-f7df3082efa0︡{"stdout":"Numerical solution: -1.4388637356\n"}︡{"stdout":"Interval containing solution: [-1.43886385 .. -1.43886363]\n"}︡{"done":true}︡
︠6b519a3b-e2dc-4b61-9997-c31843c02925i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡fa262e01-c3cc-4136-8e4c-9d36bbd28e11︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠16a03c0e-dfc8-49fb-8091-8f6544a5c7d8s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡8599e827-e09b-45a8-9be8-0eecf3c84146︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\xi}}{4 \\, \\xi^{3}} + \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} + 2 \\, \\xi}{4 \\, \\xi^{3}} \\geq 0.1686591$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi - 3\\right)} e^{\\left(2 \\, \\xi\\right)} + 4 \\, \\xi + 4\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)} + 4 \\, \\xi + 4}{4 \\, \\xi^{3}} \\geq 0.5809738$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left(4 \\, \\xi - 3\\right)} e^{\\left(3 \\, \\xi\\right)} - 3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 4}{8 \\, \\xi^{3}} + \\frac{3 \\, \\xi e^{\\xi} - 2 \\, \\xi - 4}{8 \\, \\xi^{3}} \\geq 0.1915944$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(3 \\, \\xi - 4\\right)} e^{\\left(2 \\, \\xi\\right)} - 2 \\, {\\left(\\xi + 2\\right)} e^{\\xi} + 8 \\, \\xi + 8\\right)} e^{\\left(-\\xi\\right)}}{8 \\, \\xi^{3}} + \\frac{2 \\, {\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)} - 3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 2 \\, \\xi - 4}{8 \\, \\xi^{3}} \\geq 0.9412277$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠d711da0f-9535-4ba1-b871-1dae57a33f94i︠
%md 
------------------
#### No. 31: Degree $5$ / Singularity type $A_2$. <a id="no31"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{0,u\},\; \Phi_\infty(u)= \min \{0,u\},\; \Phi_1(u) = \min \{0,\frac{-u-1}{2}\}$
︡a83ca57c-6368-4411-b995-9530e6f348c2︡{"done":true,"md":"------------------\n#### No. 31: Degree $5$ / Singularity type $A_2$. <a id=\"no31\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{0,u\\},\\; \\Phi_\\infty(u)= \\min \\{0,u\\},\\; \\Phi_1(u) = \\min \\{0,\\frac{-u-1}{2}\\}$"}
︠12725ef3-790b-407c-8995-dd8c3cb4899fs︠

# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,u)
Phi2(u)=min_symbolic(0,-u)
Phi3(u)=min_symbolic(0,1/2*u-1/2)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡93c270c7-3a58-45fe-ad9d-49d04cedc764︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, -u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, \\frac{1}{2} \\, u - \\frac{1}{2}\\right)$</div>"}︡{"done":true}︡
︠4b1f75ad-e62d-4e5b-a7c9-f1ca2f69091di︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡f8b0578b-7605-408d-b9de-e1c8f38b619f︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠26a822a8-95ab-4163-bc6b-6db8f62faf1ds︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡cc92de81-da93-4674-b4f9-6410a0139168︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(\\xi - 2\\right)} e^{\\left(2 \\, \\xi\\right)} + 3 \\, \\xi - 4 \\, e^{\\xi} + 6\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}} - \\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2}{\\xi^{3}}$</div>"}︡{"done":true}︡
︠3a315645-1047-4451-b36a-be09553de4e5i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡a1e81b68-1d9c-452c-900b-4dfc7686091b︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠55885e3b-9728-44ce-a173-6eb96d87b4cds︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.000001
xi_2=xi_1+0.000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) > 0 and RIF(F(xi_2)) < 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡a3f7fd58-d964-4740-a5f2-036ba9946b33︡{"stdout":"Numerical solution: -0.242677720874\n"}︡{"stdout":"Interval containing solution: [-0.242678725 .. -0.242676720]\n"}︡{"done":true}︡
︠42ae72ba-c822-4465-8b2f-2776b147b279i︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡50601610-b643-46af-9765-4db1f3736f32︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠fbd18133-e9a8-4c8e-b583-a54fe6a40f95s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡a8647a09-4ef7-413d-a9f8-2ff6d1a51f7e︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi e^{\\left(2 \\, \\xi\\right)} - 2 \\, {\\left(5 \\, \\xi - 2\\right)} e^{\\xi} - 6\\right)} e^{\\left(-\\xi\\right)}}{8 \\, \\xi^{3}} - \\frac{4 \\, {\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} + 3 \\, {\\left(\\xi - 2\\right)} e^{\\xi} - 6 \\, \\xi - 4}{8 \\, \\xi^{3}} \\geq 0.4280779$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(5 \\, \\xi - 4\\right)} e^{\\left(2 \\, \\xi\\right)} + 6 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 12 \\, \\xi + 18\\right)} e^{\\left(-\\xi\\right)}}{8 \\, \\xi^{3}} + \\frac{4 \\, {\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - {\\left(\\xi - 2\\right)} e^{\\xi} - 10 \\, \\xi - 12}{8 \\, \\xi^{3}} \\geq 0.4278612$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 4 \\, {\\left(\\xi - 1\\right)} e^{\\xi} - 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2 \\, \\xi - 2}{2 \\, \\xi^{3}} \\geq \\left(-0.1469232\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left({\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 2 \\, {\\left(\\xi - 4\\right)} e^{\\xi} + 6 \\, \\xi + 9\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} + {\\left(\\xi - 2\\right)} e^{\\xi} - \\xi}{2 \\, \\xi^{3}} \\geq 0.7109140$</div>"}︡{"stdout":"Surface is not stable!\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left({\\left(2 \\, \\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} - 4 \\, {\\left(\\xi - 1\\right)} e^{\\xi} - 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2 \\, \\xi - 2}{2 \\, \\xi^{3}} \\leq \\left(-0.1458051\\right)$</div>"}︡{"done":true}︡
︠0f43dd2f-d350-4813-80be-ad8f8bab5c3fi︠
%md 
------------------
#### No. 32: Degree $5$ / Singularity type $A_1$. <a id="no32"></a>
The combinatorial data is is given by $\Box=[-1,1]$ and $\Phi_0(u) = \min \{0,u\},\; \Phi_\infty(u)= \min \{0,u\},\; \Phi_1(u) = \min \{-u,0\}$
︡bb2fac93-1c90-4df4-b213-0e55e5b3546a︡{"done":true,"md":"------------------\n#### No. 32: Degree $5$ / Singularity type $A_1$. <a id=\"no32\"></a>\nThe combinatorial data is is given by $\\Box=[-1,1]$ and $\\Phi_0(u) = \\min \\{0,u\\},\\; \\Phi_\\infty(u)= \\min \\{0,u\\},\\; \\Phi_1(u) = \\min \\{-u,0\\}$"}
︠37bc7c67-e452-4582-9c0e-d16ff9238b2fs︠

# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,u)
Phi2(u)=min_symbolic(0,u)
Phi3(u)=min_symbolic(-u,0)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡eb0ac451-10c8-468b-b0b5-04e6c63e1c05︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-u, 0\\right)$</div>"}︡{"done":true}︡
︠dccab024-1a98-40e2-b8de-d5474b0eee0ai︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡52c39dc5-e072-4703-8990-9016ae2d87b0︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠f758bd8b-8813-40f1-851d-14725be1c784s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡086df20e-2ff0-4541-a0fd-11fb5cc815db︡{"html":"<div align='center'>$\\displaystyle -\\frac{2 \\, {\\left(\\xi - 2 \\, e^{\\xi} + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} - \\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2}{\\xi^{3}}$</div>"}︡{"done":true}︡
︠fb3518fc-c919-4c08-bfea-7e2f4716a6bai︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡cbc12133-90e4-4b1d-a895-97454cbf3d86︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠b0aca5be-0109-423d-b132-d8700cc72b23s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_2=sol-0.0000001
xi_1=xi_2+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡fa106877-1da0-4fbc-ae09-7c83bcd30af9︡{"stdout":"Numerical solution: -0.276432541617\n"}︡{"stdout":"Interval containing solution: [-0.276432649 .. -0.276432439]\n"}︡{"done":true}︡
︠9f0e4444-e7e5-462d-8736-497fb59e21abi︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡f7e38114-5585-42a2-bf44-5f64d3f29cbc︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠49ac8f67-384f-4a3a-8b60-e101e1f6bebds︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡20303238-5875-4c22-a2f4-9c9c81ba8e5f︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2 \\, \\xi - 2}{2 \\, \\xi^{3}} \\geq 0.1917138$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2 \\, \\xi - 2}{2 \\, \\xi^{3}} \\geq 0.1917138$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left({\\left(\\xi - 2\\right)} e^{\\xi} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} + \\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2 \\, \\xi - 2}{2 \\, \\xi^{3}} \\geq 0.1916600$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left({\\left(\\xi - 2\\right)} e^{\\xi} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} - \\frac{{\\left(3 \\, \\xi^{2} - 4 \\, \\xi + 2\\right)} e^{\\left(3 \\, \\xi\\right)} - 2 \\, \\xi - 2}{2 \\, \\xi^{3}} \\geq 0.5751334$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠c3b0253c-4950-4975-b651-72a414107ba1i︠
%md 
------------------
#### No. 33: Degree $6$ / Singularity type $A_2$. <a id="no33"></a>
The combinatorial data is is given by $\Box=[-1,3]$ and $\Phi_0(u) = \min \{0,u\},\; \Phi_\infty(u)= \min \{0,u\},\; \Phi_1(u) = \frac{-u-1}{2}$
︡d3422779-d302-4d20-9003-7181493033e5︡{"done":true,"md":"------------------\n#### No. 33: Degree $6$ / Singularity type $A_2$. <a id=\"no33\"></a>\nThe combinatorial data is is given by $\\Box=[-1,3]$ and $\\Phi_0(u) = \\min \\{0,u\\},\\; \\Phi_\\infty(u)= \\min \\{0,u\\},\\; \\Phi_1(u) = \\frac{-u-1}{2}$"}
︠3f4adce1-ed7f-47d6-b34d-f895c5b0d5d5s︠

# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,3]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(0,u)
Phi2(u)=min_symbolic(0,u)
Phi3(u)=-1/2*u-1/2

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡475dea06-1172-46a0-b983-d3f60f7d273c︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 3$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ -\\frac{1}{2} \\, u - \\frac{1}{2}$</div>"}︡{"done":true}︡
︠e23880b6-b5ea-4b99-9ca4-a53c742a7858i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡ed8285b8-417c-4075-9f3b-09867a843d9c︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠909c48e0-49b1-4c21-b9cb-eb7a44a2c821s︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡93fb3c33-bc07-44ab-9af8-733053fb7578︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(3 \\, \\xi - 2\\right)} e^{\\left(3 \\, \\xi\\right)}}{2 \\, \\xi^{3}} - \\frac{{\\left(3 \\, \\xi - 8 \\, e^{\\xi} + 6\\right)} e^{\\left(-\\xi\\right)}}{2 \\, \\xi^{3}}$</div>"}︡{"done":true}︡
︠c0d91c6c-2350-40a7-946f-432673d9d772i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡4f61d62e-668b-4af2-a7e3-dad6ed85fe24︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠0c0bd79d-4781-42b3-854a-99e38213bd70s︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡719ef2c0-5268-4325-ac4d-364693176683︡{"stdout":"Numerical solution: -1.24607304295\n"}︡{"stdout":"Interval containing solution: [-1.24607316 .. -1.24607291]\n"}︡{"done":true}︡
︠be552219-9f5c-4991-ba84-b3215f1b9b4bi︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡a2d6bf4a-8964-4f0d-baa0-b91f3f345212︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠fced5a29-54a7-418e-be57-051e32629338s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡b7405f96-b077-45a2-bd4e-8381652fc05e︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.5195618$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} - \\frac{{\\left(2 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 3\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 0.5195618$</div>"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(8 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 15\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq \\left(-0.03507074\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(4 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 9\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\geq 1.004053$</div>"}︡{"stdout":"Surface is not stable!\n"}︡{"html":"<div align='center'>$\\displaystyle -\\frac{{\\left(2 \\, \\xi - 1\\right)} e^{\\left(3 \\, \\xi\\right)}}{4 \\, \\xi^{3}} + \\frac{{\\left(8 \\, {\\left(\\xi - 2\\right)} e^{\\xi} + 6 \\, \\xi + 15\\right)} e^{\\left(-\\xi\\right)}}{4 \\, \\xi^{3}} \\leq \\left(-0.03506876\\right)$</div>"}︡{"done":true}︡
︠fe84bf90-8961-4d36-ac90-b5458dfc1fdbi︠
%md 
------------------
#### No. 34: Degree $6$ / Singularity type $A_1$. <a id="no34"></a>
The combinatorial data is is given by $\Box=[-1,2]$ and $\Phi_0(u) = \min \{-u,0\},\; \Phi_\infty(u)= \min \{0,u\},\; \Phi_1(u) = \min \{0,u\}$
︡22844a11-ca35-459b-97d9-c940ff511d3e︡{"done":true,"md":"------------------\n#### No. 34: Degree $6$ / Singularity type $A_1$. <a id=\"no34\"></a>\nThe combinatorial data is is given by $\\Box=[-1,2]$ and $\\Phi_0(u) = \\min \\{-u,0\\},\\; \\Phi_\\infty(u)= \\min \\{0,u\\},\\; \\Phi_1(u) = \\min \\{0,u\\}$"}
︠7fccd481-6802-4a8c-a416-2a35569085f0s︠

# We are working with interval arithmetic with precision of 26 bit
# this fixes the precision e.g. for evaluations of exponential functions
RIF=RealIntervalField(26)

# The base polytope/interval 
B=[-1,2]

# 3 PL functions on the interval
Phi1(u)=min_symbolic(-u,0)
Phi2(u)=min_symbolic(0,u)
Phi3(u)=min_symbolic(0,u)

# the degree of \bar Phi
degPhi=Phi1+Phi2+Phi3+2

print "The combinatorial data"
show(B)
show(Phi1)
show(Phi2)
show(Phi3)
︡bdfbff3b-c306-4004-82b8-2f001e8f8f02︡{"stdout":"The combinatorial data\n"}︡{"html":"<div align='center'>[$\\displaystyle -1$, $\\displaystyle 2$]</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(-u, 0\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"html":"<div align='center'>$\\displaystyle u \\ {\\mapsto}\\ \\min\\left(0, u\\right)$</div>"}︡{"done":true}︡
︠b3133fde-72cd-47fc-a248-0d4ce8c07227i︠
%md
#### Step (i) -- obtain a closed form for $F_{X,\xi}(1)$
For this we have to analytically solve the integral $\int_\Box u \deg \bar \Phi (u) e^{\xi u} du$.
︡a58f03be-42c6-46b8-a525-2c2da83ef8ba︡{"done":true,"md":"#### Step (i) -- obtain a closed form for $F_{X,\\xi}(1)$\nFor this we have to analytically solve the integral $\\int_\\Box u \\deg \\bar \\Phi (u) e^{\\xi u} du$."}
︠192e28a4-1b8f-4ae6-8c93-65fb34cd93fes︠
# The integral can be solved symbolically:
F(xi)=integral(degPhi(u)*u*exp(xi*u),u,-1,B[1])
show(F(xi))
︡7f4fa4b8-b0ec-4fbc-8e17-ea9342726417︡{"html":"<div align='center'>$\\displaystyle -\\frac{2 \\, {\\left(\\xi - 2 \\, e^{\\xi} + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} + \\frac{2 \\, {\\left({\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + 1\\right)}}{\\xi^{3}}$</div>"}︡{"done":true}︡
︠4d6d80e0-6edf-4cfc-90b9-01b3da6c9167i︠
%md
#### Step (ii) -- find an estimate for the soliton candidate vector field $\xi$
︡7372d455-6474-4d99-a120-b13eb1d53b73︡{"done":true,"md":"#### Step (ii) -- find an estimate for the soliton candidate vector field $\\xi$"}
︠104529ff-f192-40ac-8703-29de53f9e54cs︠
# Solve numerically
sol=find_root(F,-10,10)
print "Numerical solution:",sol

# Choose upper and lower bound and
# hope that the exact solution lies in between
xi_1=sol-0.0000001
xi_2=xi_1+0.0000002

#define a real value xi0 between xi_1 and xi_2
# representing the exact solution
xi0=RIF(xi_1,xi_2)

# Check whether Intermediate value theorem guarantees a zero beween
# xi_1 and xi_2, i.e evaluate F at xi_1 and xi_2 using interval arithmetic
if RIF(F(xi_1)) < 0 and RIF(F(xi_2)) > 0:
       print "Interval containing solution:", xi0.str(style='brackets')
︡e97d28b3-b671-4ae3-9923-9e653d5bd74e︡{"stdout":"Numerical solution: -0.97052220918\n"}︡{"stdout":"Interval containing solution: [-0.970522315 .. -0.970522105]\n"}︡{"done":true}︡
︠07c2f57e-02ee-4c91-879d-53b9aa3cb57ei︠
%md
#### Step (iii) & (iv) -- obtain closed forms for $\operatorname{DF}_\xi(X_{y,0,1})$ and plug in the estimate for $\xi$
For every choice of $y \in \mathbb P^1$ we have to  symbolically solve the integrals 
$\int_\Box   e^{u\xi} \cdot \left((1+\Phi_y(u))^2  - (1+\sum_{z\neq y} \Phi_z(u))^2\right) \; du$, which up to scaling with a positive constant coincide with $\operatorname{DF}_\xi(\mathcal{X}_{y,0,1})$.
Then we plug in the estimate for $\xi$ into the resulting expression.
︡480424d9-7c24-4b91-978e-559d791ee6bc︡{"done":true,"md":"#### Step (iii) & (iv) -- obtain closed forms for $\\operatorname{DF}_\\xi(X_{y,0,1})$ and plug in the estimate for $\\xi$\nFor every choice of $y \\in \\mathbb P^1$ we have to  symbolically solve the integrals \n$\\int_\\Box   e^{u\\xi} \\cdot \\left((1+\\Phi_y(u))^2  - (1+\\sum_{z\\neq y} \\Phi_z(u))^2\\right) \\; du$, which up to scaling with a positive constant coincide with $\\operatorname{DF}_\\xi(\\mathcal{X}_{y,0,1})$.\nThen we plug in the estimate for $\\xi$ into the resulting expression."}
︠283855c6-8cfb-4eed-938e-de38a8a4c0f8s︠
#check for positivity of the DF invariant for our vector field xi
print("Stability test for test configurations:")

#storage for DF values (as intervals)
IDF={}

DF1(xi)=integral(exp(xi*u)*((Phi1+1)(u)^2-(Phi3+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[1]=RIF(DF1(xi0)) # evaluate DF1 at xi0 using interval arithmetic
show(DF1(xi) >= IDF[1].lower())

DF2(xi)=integral(exp(xi*u)*((Phi2+1)(u)^2-(Phi1+Phi3+1)(u)^2)/2,u,-1,B[1])
IDF[2]=RIF(DF2(xi0))
show(DF2(xi) >= IDF[2].lower())

DF3(xi)=integral(exp(xi*u)*((Phi3+1)(u)^2-(Phi1+Phi2+1)(u)^2)/2,u,-1,B[1])
IDF[3]=RIF(DF3(xi0))
show(DF3(xi) >= IDF[3].lower())

DF4(xi)=integral(exp(xi*u)*((1)^2-(Phi3+Phi2+Phi1+1)(u)^2)/2,u,-1,B[1])
IDF[4]=RIF(DF4(xi0))
show(DF4(xi) >= IDF[4].lower())

if all([DF > 0 for DF in IDF.values()]): 
    print("Surface is stable!")
else:
    if any([DF <= 0 for DF in IDF.values()]):
        print("Surface is not stable!")
        
        # print upper bound for destablising DF value
        if IDF[1] <= 0: show(DF1(xi) <= IDF[1].upper())
        if IDF[2] <= 0: show(DF2(xi) <= IDF[2].upper())
        if IDF[3] <= 0: show(DF3(xi) <= IDF[3].upper())
        if IDF[4] <= 0: show(DF4(xi) <= IDF[4].upper())
    else:
        print("Cannot determine stability")
        # Note, that "not a > 0" does not imply "a <= 0" for intervals
︡ebc073bc-42f8-4568-adeb-818c1a95c547︡{"stdout":"Stability test for test configurations:\n"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left({\\left(\\xi - 2\\right)} e^{\\xi} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} - \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1}{\\xi^{3}} \\geq 0.2771940$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1}{\\xi^{3}} \\geq 0.2771960$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1}{\\xi^{3}} \\geq 0.2771960$</div>"}︡{"html":"<div align='center'>$\\displaystyle \\frac{2 \\, {\\left({\\left(\\xi - 2\\right)} e^{\\xi} + \\xi + 2\\right)} e^{\\left(-\\xi\\right)}}{\\xi^{3}} + \\frac{{\\left(\\xi - 1\\right)} e^{\\left(2 \\, \\xi\\right)} + \\xi + 1}{\\xi^{3}} \\geq 0.8315867$</div>"}︡{"stdout":"Surface is stable!\n"}︡{"done":true}︡
︠33e06f30-77eb-4319-80a4-cda96ebd6b19s︠
︡5c5d8f38-eedb-4e71-b190-2771012306af︡{"done":true}︡
︠46c85731-c84f-4cda-a39f-36da1cf70d28︠









